//! A practical test for our `#![no_std]` and no-`alloc` support (requires Linux on amd64).
//!
//! ```sh
//! $ ( cd nonono; cargo run )
//! ```
//!
//! `#![no_std]` only prevents the containing crate from being linked with `std`, and only by
//! default. It has no effect on dependencies (see the [cargo-nono] crate), and we are also free to
//! explicitly declare `extern crate alloc` or even `extern crate std`.
//!
//! [cargo-nono]: https://github.com/hobofan/cargo-nono
//!
//! This program decodes and prints a simple DNS message, while failing to compile if nonymous or
//! any of its dependencies require `alloc` or `std`. It was adapted from the resources below:
//! * <https://fasterthanli.me/series/making-our-own-executable-packer/part-12>
//! * <https://os.phil-opp.com/set-up-rust/>
//!
//! Errors for inadvertent `alloc` dependency:
//! * error: no global memory allocator found but one is required; link to std or add `#[global_allocator]` to a static item that implements the GlobalAlloc trait.
//! * error: `#[alloc_error_handler]` function required, but not found.
//!
//! Errors for inadvertent `std` dependency:
//! * error\[E0152]: found duplicate lang item `eh_personality`
//! * error\[E0152]: found duplicate lang item `panic_impl`

#![no_std]
#![no_main]
#![feature(lang_items, naked_functions)]

#[macro_export]
macro_rules! print { ($($arg:tt)*) => { drop(write!(Stdout::default(), $($arg)*)) } }
#[macro_export]
macro_rules! println { ($($arg:tt)*) => { drop(writeln!(Stdout::default(), $($arg)*)) } }

// Define memcpy(3) and memset(3).
extern crate rlibc;

use core::arch::asm;
use core::fmt::{self, Write};

use nonymous::view::{Message, MessageError, View};
use nonymous::fmt::Plain;

/// Decodes a simple DNS message, then prints it in zone format.
pub unsafe fn main() -> Result<(), MessageError> {
    let source = b"\x13\x13\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    let (message, _) = Message::view(source, ..)?;
    println!("{}", Plain(&message));

    Ok(())
}

/// Calls [`main`] and either panics (with error message) or exits with zero.
#[no_mangle]
pub unsafe fn _main() -> ! {
    if let Err(e) = main() {
        println!("Fatal error in main: {}", e);
        println!("{:?}", e);
        panic!();
    } else {
        exit(0);
    }
}

/// Writer for stdout (fd 1).
///
/// The most recent write(2) return value (if any) is stored inside, as `Ok(len)` for
/// successful writes of `len` bytes, or `Err(errno)` negated into a positive number.
#[derive(Default)]
pub struct Stdout(pub Option<Result<usize, usize>>);

impl Write for Stdout {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        unsafe {
            self.0 = Some(match write(1, s.as_ptr(), s.len()) as isize {
                x if x < 0 => Err(x.wrapping_neg() as usize),
                x => Ok(x as usize),
            });

            match self.0 {
                Some(Ok(x)) if x == s.len() => Ok(()),
                _ => Err(fmt::Error),
            }
        }
    }
}

#[lang = "eh_personality"]
pub fn eh_personality() {}

/// Prints the panic details and exits with 101, like the `std` panic handler.
#[panic_handler]
pub unsafe fn panic(info: &core::panic::PanicInfo) -> ! {
    println!("Fatal error: {}", info);
    exit(101);
}

/// Naked entry point with C ABI.
///
/// Our rsp is guaranteed to be 16-byte aligned at the start of _start by the System V amd64
/// ABI, but Rust functions have a prologue that expects rsp to be misaligned by 8 bytes,
/// pushing a dummy word to compensate. If _start was a Rust function, instructions that
/// require an aligned rsp like movaps will cause the process to segfault.
///
/// The prologue does that because, despite rsp being kept 16-byte aligned whenever
/// possible, the call instruction (which would normally precede each function’s execution)
/// pushes the return address (a qword) onto the stack, which misaligns rsp.
///
/// By calling the initial Rust function from a non-Rust function without that requirement,
/// we give the prologue what it needs to ensure that rsp is aligned in the function body.
#[no_mangle]
#[naked]
pub unsafe extern "C" fn _start() {
    asm!("mov rdi, rsp", "call _main", options(noreturn))
}

/// Wrapper around exit(2).
pub unsafe fn exit(code: i32) -> ! {
    asm!(
        "syscall",
        in("rax") 60usize,                      // syscall#
        in("rdi") code,                         // arg1
        options(noreturn),                      // no inout/lateout for rax/rdx/rcx/r11
    );
}

/// Wrapper around write(2).
pub unsafe fn write(fd: u32, buf: *const u8, count: usize) -> usize {
    let mut retval;

    asm!(
        "syscall",
        inout("rax") 1usize => retval,          // syscall#; retval/-errno
        in("rdi") fd,                           // arg1
        in("rsi") buf,                          // arg2
        inout("rdx") count => _,                // arg3; retval2
        lateout("rcx") _,                       // clobbered by syscall instruction
        lateout("r11") _,                       // clobbered by syscall instruction
        options(nostack),                       // Linux syscalls don’t touch the stack at
                                                // all, so we don’t care about its alignment
    );

    retval
}

/// Needed when `profile.*.panic` is not `abort`.
///
/// We are in a workspace, and “`panic` may not be specified in a `package` profile”.
#[allow(non_snake_case)]
#[no_mangle]
pub unsafe fn _Unwind_Resume() {
    // empty (but probably the wrong implementation)
}
