package bam;

import java.nio.file.Path;
import java.util.stream.Stream;

import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;

class Docker {
    static String IMAGE = "delan/nonymous:1.57.0_nightly-2022-09-17";

    static Job job(final String key, final String name, final Task<?,?>... tasks) {
        return new Job(name, key)
            .dockerConfiguration(new DockerConfiguration().image(IMAGE))
            .tasks(tasks);
    }

    static Job hostJob(final String key, final String name, final Task<?,?>... tasks) {
        return new Job(name, key)
            .requirements(new Requirement("system.docker.executable"))
            .tasks(tasks);
    }

    static Job tarpaulin(String key, String name) {
        var cargo = Rv.MSRV.toolchain(Os.LINUX, Arch.X64, true, true).cargo();
        var args = Stream.<String>builder();
        args.add("cargo").add("tarpaulin");
        args.add("--no-default-features");
        cargo.allowedFeatures(args);
        args.add("--skip-clean");
        args.add("--exclude-files").add("bore");
        args.add("--exclude-files").add("nonono");
        args.add("--out").add("Xml");

        return new Job(name, key)
            .artifacts(new Artifact()
                .name("cobertura.xml")
                .copyPattern("cobertura.xml")
                .shared(true).required(true))
            .tasks(new VcsCheckoutTask().addCheckoutOfDefaultRepository())
            .tasks(new ScriptTask().inlineBodyFromPath(Path.of("src/main/resources/tarpaulin.sh")))
            .tasks(new DockerRunContainerTask()
                .imageName(IMAGE)
                .containerCommand(String.join(" ", args.build().toArray(String[]::new)))
                .additionalArguments("--security-opt seccomp=.bamboo/seccomp.json"));
    }
}
