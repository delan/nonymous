package bam;

import static java.lang.String.format;

import java.util.function.BiConsumer;
import java.util.stream.Stream;

import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;

class Toolchain {
    Os os;
    Arch arch;
    Rv rv;
    boolean isDocker;
    boolean isRustup;

    Toolchain(Os os, Arch arch, Rv rv) {
        this.os = os;
        this.arch = arch;
        this.rv = rv;
        this.isDocker = false;
        this.isRustup = true;
    }

    Toolchain(Os os, Arch arch, Rv rv, boolean isDocker, boolean isRustup) {
        this(os, arch, rv);
        this.isDocker = isDocker;
        this.isRustup = isRustup;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Toolchain))
            return false;
        if (!os.equals(((Toolchain) other).os))
            return false;
        if (!arch.equals(((Toolchain) other).arch))
            return false;
        if (!rv.equals(((Toolchain) other).rv))
            return false;
        return true;
    }

    String rustup() {
        return format("%s-%s", rv.version.get(), arch.target);
    }

    Cargo cargo() {
        return new Cargo(this);
    }

    Job job(String key, String name) {
        var result = (isDocker ? Docker.job(key, name) : new Job(name, key))
            .tasks(new VcsCheckoutTask().addCheckoutOfDefaultRepository());

        if (!isDocker)
            result.requirements(Requirement.equals(".os", os.requirement));

        return result;
    }

    Job job(String key, String name, BiConsumer<Stream.Builder<Task>, Cargo> tasks) {
        var builder = Stream.<Task>builder();
        tasks.accept(builder, cargo());

        var result = job(key, name)
            .tasks(builder.build().toArray(Task[]::new));

        if (!isDocker)
            result.requirements(Requirement.equals(".os", os.requirement));

        return result;
    }
}
