package bam;

class Os {
    static Os LINUX = new Os("linux", "Linux");
    static Os WINDOWS = new Os("windows", "Windows");
    static Os OPENBSD = new Os("openbsd", "OpenBSD");

    String id;
    String name;
    String requirement;

    Os(String id, String name) {
        this.id = id;
        this.name = name;
        this.requirement = id;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Os))
            return false;
        return id.equals(((Os) other).id);
    }
}
