package bam;

import static java.lang.String.format;

import java.nio.file.Path;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.condition.AnyTaskCondition;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.ArtifactSubscription;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.Dependencies;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;

/**
 * Plan configuration for Bamboo.
 *
 * @see <a href="https://confluence.atlassian.com/display/BAMBOO/Bamboo+Specs">Bamboo Specs</a>
 */
@BambooSpec
public class PlanSpec {
    static final BambooServer INSTANCE = new BambooServer("https://bamboo.daz.cat");
    static final String REPO_WEB = "https://bitbucket.org/delan/nonymous";

    boolean production;
    String projectKey;
    String projectName;
    String repoName;
    Project project;

    public static void main(final String[] args) throws Exception {
        var env = System.getenv("BAMBOO_SPECS_ENV");
        final var spec = new PlanSpec(env != null && env.equals("production"));
        spec.publish(spec.testCoverage());
        // publish COMM plan next (depends on TCOV)
        spec.publish(spec.commitChecks());
        // publish RELE plan next (will eventually depend on COMM?)
        spec.publish(spec.release());
        // publish DI plan (independent)
        spec.publish(spec.dockerImage());
    }

    PlanSpec(boolean production) {
        this.production = production;
        var key = "NO";
        var name = "nonymous";
        this.projectKey = production ? key : "Z" + key;
        this.projectName = production ? name : name + " (dev)";
        this.repoName = production ? name : name + " (no build status)";
        this.project = new Project().key(projectKey).name(projectName);
    }

    void publish(final Plan plan) {
        INSTANCE.publish(plan);
        INSTANCE.publish(defaultPlanPermissions(plan.getIdentifier()));
    }

    Plan plan(String key, String name, boolean triggerAndBranch, Stage... stages) {
        var plan = new Plan(project, name, key)
            .description(format("Bamboo Specs: %s", REPO_WEB))
            .linkedRepositories(repoName)
            .stages(stages);

        if (triggerAndBranch && production) {
            plan.triggers(new PushTrigger());
            plan.planBranchManagement(new PlanBranchManagement().createForVcsBranch());
        }

        return plan;
    }

    Stage stage(final String name, final Job... jobs) {
        return new Stage(name).jobs(jobs);
    }

    @SafeVarargs final <T extends Task<T,P>, P extends TaskProperties>
    Job job(final String key, final String name, final T... tasks) {
        return new Job(name, key).tasks(tasks);
    }

    PlanPermissions defaultPlanPermissions(PlanIdentifier planIdentifier) {
        final var permissions = new Permissions()
                .userPermissions("delan", PermissionType.ADMIN)
                .groupPermissions("bamboo-admin", PermissionType.ADMIN)
                .loggedInUserPermissions(PermissionType.VIEW)
                .anonymousUserPermissionView();

        return new PlanPermissions(planIdentifier)
                .permissions(permissions);
    }

    Plan commitChecks() {
        var stage = stage(
            "default",
            Rv.MSRV.toolchain(Os.LINUX, Arch.X64, true, true)
                .job("MSRV", "Linux (x64 MSRV)", (result, cargo) -> {
                    cargo.addCommitChecks(result);
                }),
            Rv.NIGHTLY.toolchain(Os.LINUX, Arch.X64, true, true)
                .job("NIGHTLY", "Linux (x64 nightly)", (result, cargo) -> {
                    cargo.addCommitChecks(result);
                }),
            Rv.MSRV.toolchain(Os.WINDOWS, Arch.X64, false, true)
                .job("WIN64", "Windows (x64)", (result, cargo) -> {
                    cargo.addCommitChecks(result);
                }).enabled(false), // FIXME badapple agent broken
            Rv.MSRV.toolchain(Os.WINDOWS, Arch.X86, false, true)
                .job("WIN32", "Windows (x86)", (result, cargo) -> {
                    cargo.addCommitChecks(result);
                }).enabled(false), // FIXME badapple agent broken
            Rv.DEFAULT.toolchain(Os.OPENBSD, Arch.X64, false, false)
                .job("OPENBSD", "OpenBSD (x64)", (result, cargo) -> {
                    cargo.addCommitChecks(result);
                }));

        return plan("COMM", "Commit checks", true, stage)
            .dependencies(new Dependencies().childPlans(new PlanIdentifier(projectKey, "TCOV")));
    }

    Plan testCoverage() {
        var calculate = stage("calculate", Docker.tarpaulin("CALC", "Calculate"));

        var uploadTask = new ScriptTask().inlineBodyFromPath(Path.of("src/main/resources/TCOV-SEND.sh"));
        if (production)
            uploadTask.argument("-t ${bamboo..codecov.secret}");
        else
            uploadTask.argument("-d");

        // Codecov Uploader is a Linux x64 binary
        var uploadJob = Rv.MSRV.toolchain(Os.LINUX, Arch.X64, true, true)
            .job("SEND", "Upload")
            .tasks(uploadTask)
            .artifactSubscriptions(new ArtifactSubscription().artifact("cobertura.xml"));
        if (!production)
            uploadJob.name("Upload (dry run)");

        var upload = stage("upload", uploadJob);
        return plan("TCOV", "Test coverage", true, calculate, upload);
    }

    Plan release() {
        var variableValue = "don’t release";
        var nonymousVariable = ".nonymous.version";
        var boreVariable = ".bore.version";
        var nonymous = releaseStage("NONYMOUS", "nonymous", ".", nonymousVariable, variableValue);
        var bore = releaseStage("BORE", "bore", "bore", boreVariable, variableValue);

        return plan("RELE", "Release", false, nonymous, bore).variables(
            new Variable(nonymousVariable, variableValue),
            new Variable(boreVariable, variableValue));
    }

    Plan dockerImage() {
        var variableValue = "don’t build";
        var msrvVariable = ".rustup.msrv";
        var build = stage("build", Docker.hostJob("BUILD", "build",
            stopIfVariableEquals(true, msrvVariable, variableValue),
            new VcsCheckoutTask().addCheckoutOfDefaultRepository(),
            new ScriptTask().inlineBodyFromPath(Path.of("src/main/resources/DI.sh"))
                .argument(String.join(" ", new String[] {
                    "${bamboo." + msrvVariable + "}",
                    "${bamboo..docker.user}",
                    "${bamboo..docker.secret}",
                }))));

        return plan("DI", "Docker image", false, build).variables(
            new Variable(msrvVariable, variableValue));
    }

    Stage releaseStage(String key, String cratePackageName, String cratePackagePath, String variableName, String variableValue) {
        var stop = stopIfVariableEquals(true, variableName, variableValue);
        var checkout = new VcsCheckoutTask().addCheckoutOfDefaultRepository().cleanCheckout(true);
        var script = new ScriptTask().inlineBodyFromPath(Path.of("src/main/resources/RELE.sh"))
            .argument(String.join(" ", new String[] {
                cratePackageName,
                "${bamboo." + variableName + "}",
                cratePackagePath,
                "${bamboo..cargo.secret}",
            }));

        return stage(cratePackageName, Docker.job(key, cratePackageName, stop, checkout, script));
    }

    AnyTask stopIfVariableEquals(boolean success, String variable, String value) {
        var condition = new AnyTaskCondition(new AtlassianModule("com.atlassian.bamboo.plugins.bamboo-conditional-tasks:variableCondition"))
            .configuration(new MapBuilder<String, String>().put("operation", "equals").put("variable", variable).put("value", value).build());

        return new AnyTask(new AtlassianModule("com.atlassian.bamboo.plugins.bamboo-conditional-tasks:stopExecutionTask"))
            .configuration(new MapBuilder<String, String>().put("stop.execution.is_success", String.valueOf(success)).build())
            .conditions(condition);
    }
}
