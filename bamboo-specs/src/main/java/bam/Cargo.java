package bam;

import static java.lang.String.format;

import java.util.Arrays;
import java.util.stream.Stream;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.builders.task.CommandTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;

class Cargo {
    static final String[] FEATURES = {"std", "alloc", "bench"};
    static final String STRICT = "RUSTFLAGS='--cfg ci_' RUSTDOCFLAGS='--cfg ci_'";

    Toolchain toolchain;

    Cargo(Toolchain toolchain) {
        this.toolchain = toolchain;
    }

    Task cargo(boolean strict, String... args) {
        return toolchain.isDocker ? cargoDocker(strict, args) : cargoNative(strict, args);
    }

    String cargoArgs(String... args) {
        if (toolchain.isRustup)
            return format("+%s %s", toolchain.rustup(), String.join(" ", args));
        else
            return String.join(" ", args);
    }

    Task cargoNative(boolean strict, String... args) {
        var result = new CommandTask().executable("cargo");
        result.argument(cargoArgs(args));
        if (strict)
            result.environmentVariables(STRICT);
        return result;
    }

    Task cargoDocker(boolean strict, String... args) {
        var result = new ScriptTask().inlineBody(
            "#!/bin/sh\n"
            + "set -eu\n"
            + "exec cargo \"$@\"\n");
        result.argument(cargoArgs(args));
        if (strict)
            result.environmentVariables(STRICT);
        return result;
    }

    void allFeaturesExcept(Stream.Builder<String> args, String... features) {
        if (features.length == 0) {
            args.add("--all-features");
            return;
        }

        var features_ = Arrays.asList(features);
        var result = Stream.of(FEATURES)
            .filter(x -> !features_.contains(x))
            .toArray(String[]::new);
        if (result.length > 0) {
            args.add("--features");
            args.add(String.join(",", result));
        }
    }

    void allowedFeatures(Stream.Builder<String> args) {
        if (toolchain.rv.isNightly)
            nightlyFeatures(args);
        else
            stableFeatures(args);
    }

    void stableFeatures(Stream.Builder<String> args) {
        allFeaturesExcept(args, "bench");
    }

    void nightlyFeatures(Stream.Builder<String> args) {
        allFeaturesExcept(args);
    }

    void addCommitChecks(Stream.Builder<Task> result) {
        if (toolchain.rv.version.equals(Rv.MSRV.version))
            result.add(checkFormatting());
        result.add(testNoStd());
        result.add(testStd());
        result.add(buildDocs());
        result.add(testDocs());
        if (toolchain.equals(new Toolchain(Os.LINUX, Arch.X64, Rv.NIGHTLY)))
            result.add(nonono());
        if (!toolchain.os.equals(Os.WINDOWS))
            result.add(gitDirty());
    }

    Task checkFormatting() {
        return cargo(true, "fmt", "--all", "--", "--check");
    }

    Task testNoStd() {
        return cargo(true, "test", "--no-default-features"); // NOT --workspace
    }

    Task testStd() {
        var args = Stream.<String>builder();
        args.add("test");
        args.add("--no-default-features");
        allowedFeatures(args);
        args.add("--all-targets");
        args.add("--workspace");
        return cargo(true, args.build().toArray(String[]::new));
    }

    Task testDocs() {
        var args = Stream.<String>builder();
        args.add("test");
        args.add("--no-default-features");
        allowedFeatures(args);
        args.add("--doc");
        args.add("--workspace");
        return cargo(true, args.build().toArray(String[]::new));
    }

    Task buildDocs() {
        var args = Stream.<String>builder();
        args.add("doc");
        args.add("--no-default-features");
        allowedFeatures(args);
        args.add("--no-deps");
        args.add("--document-private-items");
        args.add("--workspace");
        return cargo(true, args.build().toArray(String[]::new));
    }

    Task nonono() {
        var result = new ScriptTask().inlineBody(
            "#!/bin/sh\n"
            + "set -eu\n"
            + "cd nonono\n"
            + "exec cargo \"$@\" run\n");
        result.argument(format("+%s", toolchain.rustup()));
        result.environmentVariables(STRICT);
        return result;
    }

    Task gitDirty() {
        var result = new ScriptTask().inlineBody(
            "#!/bin/sh\n"
            + "! git status -z | tr \\\\0 \\\\n | grep ' '");
        return result;
    }
}
