package bam;

class Arch {
    static Arch X64 = new Arch("x64", "x86_64");
    static Arch X86 = new Arch("x86", "i686");

    String id;
    String name;
    String target;

    Arch(String id, String target) {
        this.id = id;
        this.name = id;
        this.target = target;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Arch))
            return false;
        return id.equals(((Arch) other).id);
    }
}
