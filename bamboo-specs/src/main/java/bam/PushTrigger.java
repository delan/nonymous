package bam;

import com.atlassian.bamboo.specs.builders.trigger.RemoteTrigger;

class PushTrigger extends RemoteTrigger {
    // https://support.atlassian.com/organization-administration/docs/ip-addresses-and-domains-for-atlassian-cloud-products/#AtlassiancloudIPrangesanddomains-OutgoingConnections
    static final String[] ranges = {
        "13.52.5.96/28",
        "13.236.8.224/28",
        "18.136.214.96/28",
        "18.184.99.224/28",
        "18.234.32.224/28",
        "18.246.31.224/28",
        "52.215.192.224/28",
        "104.192.137.240/28",
        "104.192.138.240/28",
        "104.192.140.240/28",
        "104.192.142.240/28",
        "104.192.143.240/28",
        "185.166.143.240/28",
        "185.166.142.240/28",
    };

    PushTrigger() {
        super();
        description("bb");
        triggerIPAddresses(allowlist());
    }

    String allowlist() {
        // https://confluence.atlassian.com/bamboo/triggering-a-bamboo-build-from-bitbucket-cloud-using-webhooks-873949130.html
        // bamboo.daz.cat is behind a reverse proxy, making the X-Forwarded-For header something
        // like “foo,127.0.0.1”, so both 127.0.0.1 as well as every “foo” need to be allowlisted
        return "127.0.0.1," + String.join(",", ranges);
    }
}
