package bam;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;

import io.atlassian.fugue.Option;

class Rv {
    static Rv DEFAULT = new Rv(none(), none(), false);
    static Rv MSRV = new Rv(some("MSRV"), some("1.57.0"), false);
    static Rv NIGHTLY = new Rv(some("nightly"), some("nightly"), true);

    Option<String> id;
    Option<String> name;
    Option<String> version;
    boolean isNightly;

    Rv(Option<String> name, Option<String> id, boolean isNightly) {
        this.id = id;
        this.name = name;
        this.version = id;
        this.isNightly = isNightly;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Rv))
            return false;
        return id.equals(((Rv) other).id);
    }

    Toolchain toolchain(Os os, Arch arch, boolean isDocker, boolean isRustup) {
        return new Toolchain(os, arch, this, isDocker, isRustup);
    }
}
