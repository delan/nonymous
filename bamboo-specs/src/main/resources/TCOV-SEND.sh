#!/bin/sh
set -eu
f=$(mktemp)
curl -fsSo $f https://uploader.codecov.io/v0.1.0_4844/linux/codecov
chmod +x $f
# FIXME -r required because codecov chokes on bamboo‘s ssh://-style remote URL
$f -Z -f cobertura.xml -r delan/nonymous "$@"
