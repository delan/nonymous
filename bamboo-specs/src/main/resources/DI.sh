#!/bin/sh
set -eu

msrv="$1"; shift
user="$1"; shift
token="$1"; shift

name=delan/nonymous:${msrv}_nightly
docker build pipelines/docker -t $name --build-arg rv=${msrv}
v="$(docker run --rm $name rustc +nightly --version | egrep -o '[^ ]+[)]$' | sed 's/)//')"
docker tag $name $name-$v

printf \%s "$token" | docker login -u "$user" --password-stdin
docker push $name-$v
