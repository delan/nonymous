#!/bin/sh
set -eu

package="$1"; shift
version="$1"; shift
path="$1"; shift
token="$1"; shift

cargo +1.52.0 set-version -p "$package" "$version"
cargo +1.52.0 build -p "$package"

user_name=Bamboo
user_email=bamboo\@bamboo.daz.cat
export GIT_COMMITTER_NAME="$user_name"
export GIT_COMMITTER_EMAIL="$user_email"
export GIT_AUTHOR_NAME="$user_name"
export GIT_AUTHOR_EMAIL="$user_email"

git commit -am "release $package $version [skip ci]"
git log -n 2 --pretty=oneline
git tag -am "$package $version" -- "$package-$version"

ssh_key="$(mktemp)"
ssh_hosts="$(mktemp)"
echo '${bamboo..git.ssh.secret}' | openssl base64 -d -A > "$ssh_key"
echo '${bamboo..git.ssh.host.key}' > "$ssh_hosts"
export GIT_SSH_COMMAND="ssh -vi $ssh_key -o UserKnownHostsFile=$ssh_hosts"

# use explicit remote instead of origin because dev linked repository is https
git push --tags git@bitbucket.org:delan/nonymous.git "$(git symbolic-ref HEAD)"

cargo publish --token "$token" --manifest-path "$path/Cargo.toml"
