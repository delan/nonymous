bore
====


[![crates.io](https://img.shields.io/crates/v/bore)](https://crates.io/crates/bore)


- [Release notes](https://bitbucket.org/delan/nonymous/src/default/RELEASES.md)


bore(1) is a DNS query tool based on [nonymous](https://crates.io/crates/nonymous).


    # rustup default 1.57.0
    cargo install bore
    bore --help


## OS packages


- NixOS/nixpkgs — `nix-shell -p bore`
- [OpenBSD](https://openports.se/net/bore) — `pkg_add bore`


## Features


- ☑ gathers default resolvers natively on Unix + Windows + macOS
- ☑ uses [pledge(2)] and [unveil(2)] on OpenBSD
- ☑ makes reverse DNS queries easily with `-x` (`--reverse`)
- ☑ highlights output to draw your attention (`--color` by default)
- ☑ comes with a detailed manual (`bore --man` or `man bore`)


[pledge(2)]: https://man.openbsd.org/OpenBSD-6.3/pledge.2
[unveil(2)]: https://man.openbsd.org/OpenBSD-6.4/unveil.2


## Requirements


- Rust 1.57+ (tested with 1.57.0)
- Should work on any platform including:
    - Linux
    - Windows 1511+
    - macOS 10.4+
    - OpenBSD 6.4+


### Windows notes


bore(1) currently requires [ENABLE_VIRTUAL_TERMINAL_PROCESSING](https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences), which was introduced on or near Windows build 10586.
This includes most versions of Windows 10 (1511+), or any version of Windows Server 2016 (1607+) or newer.


The accent colour, currently used to highlight question and record types, may be invisible under conhost windows with PowerShell’s default palette (dark blue background).
This is because both the background and the accent colour use the same “slot” which is normally for non-bright magenta.
If affected, you can reconfigure your colour scheme, or use `--color never`, or consider switching to [Windows Terminal](https://github.com/microsoft/terminal).
