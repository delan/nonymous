#[allow(unused_macros)]
macro_rules! itry {
    ($value:expr, $error:expr) => {{
        use ::eyre::WrapErr;
        match $value {
            Ok(x) => x,
            Err(e) => return Some(Err(e).wrap_err($error)),
        }
    }};
}

cfg_if::cfg_if! {
    if #[cfg(target_os = "macos")] {
        mod macos;
        pub(crate) use self::macos::*;
        pub(crate) type DefaultResolvers = MacosResolvers;
    } else if #[cfg(unix)] {
        mod unix;
        pub(crate) use self::unix::*;
        pub(crate) type DefaultResolvers = UnixResolvers;
    } else if #[cfg(windows)] {
        // visible for doctests
        pub mod windows;
        pub(crate) use self::windows::*;
        pub(crate) type DefaultResolvers = WindowsResolvers;
    } else {
        mod localhost;
        pub(crate) use self::localhost::*;
        pub(crate) type DefaultResolvers = LocalhostResolvers;
    }
}

use std::ffi::OsStr;
use std::net::{SocketAddr, ToSocketAddrs};
use std::num::NonZeroU16;

use eyre::bail;
use nonymous::fmt::Plain;
use nonymous::r#type;
use nonymous::view::rdata::Rdata;
use nonymous::view::Message;

pub trait Resolvers: Sized + Iterator<Item = eyre::Result<SocketAddr>> {
    fn new(port: PortChooser<NonZeroU16>) -> eyre::Result<Self>;
    fn show(&self, _sink: &mut impl std::io::Write) -> eyre::Result<()> {
        Ok(())
    }
}

pub(crate) enum ResolverChooser {
    Default(DefaultResolvers),
    Named(u16, Vec<String>, std::vec::IntoIter<SocketAddr>),
}

impl ResolverChooser {
    pub fn with_host(port: PortChooser<NonZeroU16>, host: &str) -> eyre::Result<Self> {
        let port = port.choose_or_zero(None);
        let names = vec![];
        let addresses = (host, port).to_socket_addrs()?;

        Ok(Self::Named(port, names, addresses))
    }

    pub fn replace_with(
        &mut self,
        port: PortChooser<NonZeroU16>,
        message: &Message,
    ) -> eyre::Result<()> {
        let port = port.choose_or_zero(None);
        let mut names = vec![];
        for record in message.ns() {
            if record.r#type() == r#type!("NS") {
                if let Rdata::CompressibleName(name) = record.rdata() {
                    names.push(Plain(&name).to_string());
                }
            }
        }

        if names.is_empty() {
            bail!("no more resolvers in the given Message");
        }

        let addresses = (names.remove(0), port).to_socket_addrs()?;
        *self = Self::Named(port, names, addresses);

        Ok(())
    }
}

impl Resolvers for ResolverChooser {
    fn new(port: PortChooser<NonZeroU16>) -> eyre::Result<Self> {
        Ok(Self::Default(DefaultResolvers::new(port)?))
    }
}

impl Iterator for ResolverChooser {
    type Item = eyre::Result<SocketAddr>;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Self::Default(resolvers) => resolvers.next(),
            Self::Named(port, names, addresses) => loop {
                if let Some(result) = addresses.next() {
                    return Some(Ok(result));
                }
                if names.is_empty() {
                    return None;
                }
                match (names.remove(0), *port).to_socket_addrs() {
                    Ok(x) => *addresses = x,
                    Err(e) => return Some(Err(e.into())),
                }
            },
        }
    }
}

/// Iterator over addresses for multiple names, with lazy resolution.
pub struct NamedResolvers {
    port: NonZeroU16,
    names: Vec<String>,
    addresses: Option<std::vec::IntoIter<SocketAddr>>,
}

impl Resolvers for NamedResolvers {
    fn new(port: PortChooser<NonZeroU16>) -> eyre::Result<Self> {
        Ok(Self {
            port: port.choose(None),
            names: vec![],
            addresses: None,
        })
    }
}

impl Iterator for NamedResolvers {
    type Item = eyre::Result<SocketAddr>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(addresses) = self.addresses.as_mut() {
            while let Some(address) = addresses.next() {
                return Some(Ok(address));
            }
        }

        if self.names.is_empty() {
            return None;
        }

        self.addresses = match (self.names.remove(0), self.port.into()).to_socket_addrs() {
            Ok(x) => Some(x),
            Err(e) => return Some(Err(e.into())),
        };

        self.next()
    }
}

#[derive(Clone, Copy)]
pub struct PortChooser<D> {
    override_port: Option<NonZeroU16>,
    default_port: D,
}

pub trait ChoosePort<D> {
    fn choose(self, resolver_port: Option<NonZeroU16>) -> D;
    fn choose_or_zero(self, resolver_port: Option<NonZeroU16>) -> u16;
}

impl<D> PortChooser<D> {
    pub fn new(override_port: Option<NonZeroU16>, default_port: D) -> Self {
        Self {
            override_port,
            default_port,
        }
    }
}

impl ChoosePort<NonZeroU16> for PortChooser<NonZeroU16> {
    fn choose(self, resolver_port: Option<NonZeroU16>) -> NonZeroU16 {
        self.override_port
            .or(resolver_port)
            .unwrap_or(self.default_port)
    }
    fn choose_or_zero(self, resolver_port: Option<NonZeroU16>) -> u16 {
        self.choose(resolver_port).into()
    }
}

impl ChoosePort<Option<NonZeroU16>> for PortChooser<Option<NonZeroU16>> {
    fn choose(self, resolver_port: Option<NonZeroU16>) -> Option<NonZeroU16> {
        self.override_port.or(resolver_port).or(self.default_port)
    }
    fn choose_or_zero(self, resolver_port: Option<NonZeroU16>) -> u16 {
        self.choose(resolver_port).map(|x| x.get()).unwrap_or(0)
    }
}

pub struct SockaddrDisplay(pub SocketAddr);

impl std::fmt::Display for SockaddrDisplay {
    fn fmt(&self, sink: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self.0 {
            SocketAddr::V4(address) => {
                write!(sink, "{};", address.ip())?;

                if address.port() != 0 {
                    write!(sink, " port {};", address.port())?;
                } else {
                    write!(sink, " no port;")?;
                }
            }
            SocketAddr::V6(address) => {
                write!(sink, "{};", address.ip())?;

                if address.port() != 0 {
                    write!(sink, " port {};", address.port())?;
                } else {
                    write!(sink, " no port;")?;
                }

                if address.flowinfo() != 0 {
                    write!(sink, " flowinfo {};", address.flowinfo())?;
                } else {
                    write!(sink, " no flowinfo;")?;
                }

                if address.scope_id() != 0 {
                    write!(sink, " scope_id {};", address.scope_id())?;
                } else {
                    write!(sink, " no scope_id;")?;
                }
            }
        }

        Ok(())
    }
}

/// Allow SIGPIPE to terminate the process.
#[allow(unsafe_code)]
pub fn sigpipe_default() {
    // FIXME non-POSIX platforms?
    #[cfg(unix)]
    unsafe {
        // https://github.com/rust-lang/rust/issues/46016#issuecomment-428106774
        // https://github.com/rust-lang/rust/issues/46016#issuecomment-605624865
        use nix::sys::signal::{signal, SigHandler::SigDfl, Signal::SIGPIPE};
        signal(SIGPIPE, SigDfl).expect("signal(2) failed");
    }
}

/// Enable ANSI escape codes on Windows.
pub fn enable_virtual_terminal_processing() {
    #[cfg(windows)]
    if atty::is(atty::Stream::Stdout) {
        winapi_util::console::Console::stdout()
            .expect("GetConsoleScreenBufferInfo failed on stdout")
            .set_virtual_terminal_processing(true)
            .expect("GetConsoleMode or SetConsoleMode failed on stdout");
    }
    #[cfg(windows)]
    if atty::is(atty::Stream::Stderr) {
        winapi_util::console::Console::stderr()
            .expect("GetConsoleScreenBufferInfo failed on stderr")
            .set_virtual_terminal_processing(true)
            .expect("GetConsoleMode or SetConsoleMode failed on stderr");
    }
}

pub(crate) fn expect_pledge(result: Result<(), pledge::Error>) {
    result
        .or_else(pledge::Error::ignore_platform)
        .expect("pledge(2) failed");
}

#[cfg(not(unix))]
pub(crate) fn expect_unveil(_: impl AsRef<OsStr>, _: &str) {}

#[cfg(unix)]
pub(crate) fn expect_unveil(path: impl AsRef<OsStr>, permissions: &str) {
    use std::os::unix::ffi::OsStrExt;
    match unveil::unveil(path.as_ref().as_bytes(), permissions) {
        Err(unveil::Error::NotSupported) => Ok(()),
        x => x,
    }
    .expect("unveil(2) failed");
}

#[cfg(not(unix))]
pub(crate) async fn spawn_man() -> bool {
    false
}

#[cfg(unix)]
#[allow(unsafe_code)]
pub(crate) async fn spawn_man() -> bool {
    use std::fs::File;
    use std::io::Write;
    use std::os::unix::io::FromRawFd;

    use eyre::WrapErr;
    use nix::unistd::{mkstemp, unlink};
    use tokio::process::Command;

    async fn real() -> eyre::Result<()> {
        let (fd, path) =
            mkstemp("/tmp/bore.XXXXXX").wrap_err("failed to create temporary file for manual")?;

        let mut man = unsafe { File::from_raw_fd(fd) };

        man.write_all(include_bytes!("../doc/bore.1"))
            .wrap_err("failed to write temporary file for manual")?;
        man.sync_all()
            .wrap_err("failed to sync temporary file for manual")?;

        let status = Command::new("man")
            .arg(&path)
            .status()
            .await
            .wrap_err("failed to spawn man(1)")?;

        if !status.success() {
            // FIXME exposing Debug here lacks polish
            bail!("man(1) spawned but failed: {:?}", status);
        }

        let unlink = unlink(&path);

        // don’t treat unlink(2) failure as failure to view manual page
        if let Err(e) = unlink {
            if let Some(path) = path.to_str() {
                eprintln!("Warning: failed to delete temporary file {}: {}", path, e);
                eprintln!();
            } else {
                unreachable!();
            }
        }

        Ok(())
    }

    if let Err(e) = real().await {
        eprintln!("Warning: {}", e);
        eprintln!();
        return false;
    }

    true
}
