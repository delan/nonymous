#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(unused)]
// FIXME rust-lang/rust-bindgen#1651 + rust-lang/rust#82523
#![allow(unaligned_references)]
#![allow(deref_nullptr)]

include!(concat!(env!("OUT_DIR"), "/dnsinfo.rs"));
