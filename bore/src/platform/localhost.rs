use std::{
    net::{Ipv4Addr, Ipv6Addr, SocketAddr},
    num::NonZeroU16,
};

use crate::platform::ChoosePort;

use super::{PortChooser, Resolvers};

pub(crate) struct LocalhostResolvers {
    addresses: std::vec::IntoIter<SocketAddr>,
}

impl Resolvers for LocalhostResolvers {
    fn new(port: PortChooser<NonZeroU16>) -> eyre::Result<Self> {
        let port = port.choose(None).into();

        Ok(Self {
            addresses: vec![
                SocketAddr::from((Ipv6Addr::LOCALHOST, port)),
                SocketAddr::from((Ipv4Addr::LOCALHOST, port)),
            ]
            .into_iter(),
        })
    }
}

impl Iterator for LocalhostResolvers {
    type Item = eyre::Result<SocketAddr>;

    fn next(&mut self) -> Option<Self::Item> {
        self.addresses.next().map(Ok)
    }
}

pub(crate) fn resolvers(port: PortChooser<NonZeroU16>) -> eyre::Result<impl Resolvers> {
    LocalhostResolvers::new(port)
}
