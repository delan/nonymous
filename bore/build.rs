use std::env;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    if env::var("CARGO_CFG_TARGET_OS")? != "macos" {
        return Ok(());
    }

    println!("cargo:rerun-if-changed=bindgen.h");
    println!("cargo:rustc-link-lib=framework=SystemConfiguration");

    let nix_cflags = env::var("NIX_CFLAGS_COMPILE").unwrap_or("".to_owned());
    let bindings = bindgen::Builder::default()
        .rust_target(bindgen::RustTarget::Stable_1_40)
        .detect_include_paths(true)
        .clang_args(nix_cflags.split(" "))
        .header("external/apple/dnsinfo.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("failed to generate bindings");

    let path = std::path::PathBuf::from(env::var("OUT_DIR")?);
    bindings.write_to_file(path.join("dnsinfo.rs"))?;

    Ok(())
}
