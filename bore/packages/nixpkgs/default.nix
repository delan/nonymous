# usage: nix-build | nix-shell
# https://sandervanderburg.blogspot.com/2014/07/managing-private-nix-packages-outside.html
{ system ? builtins.currentSystem }:
let
  pkgs = import <nixpkgs> { inherit system; };
  callPackage = pkgs.lib.callPackageWith (pkgs // self);
  self = {
    bore = callPackage ./bore.nix {
      inherit (pkgs.darwin) Libsystem;
      inherit (pkgs.darwin.apple_sdk.frameworks) SystemConfiguration;
    };
  };
in self
