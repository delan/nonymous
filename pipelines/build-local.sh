#!/bin/sh
# usage: pipelines/build-local.sh

set -eu

. pipelines/step.sh
. pipelines/versions.sh
export RUSTFLAGS='--cfg ci_'

# check formatting in build-1.46.sh only,
# because rustfmt’s style changes over time

step 'run tests (#![no_std])' "$@" \
    cargo test --no-default-features # NOT --workspace

step 'run tests (std)' "$@" \
    cargo test --features std --all-targets --workspace

step 'doc tests' "$@" \
    cargo test --features std --doc --workspace

step 'build docs' "$@" \
    cargo doc --features std --no-deps --document-private-items --workspace

step 'test bore --decode' sh -c ' \
    printf '\''\0\0\0\0\0\0\0\0\0\0\0\0'\'' \
    | "$@" cargo run -p bore -- --decode \
    | grep -q '\'';; NoError #0 Query 0 0 0 0 flags'\'' \
' '' "$@"

step 'test bore --show-resolvers' sh -c ' \
    "$@" cargo run -p bore -- --show-resolvers \
    | grep -q '\''effective resolver order (no --port):'\'' \
'
