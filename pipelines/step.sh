#!/bin/sh

set -eu

step() {
    label="$1"; shift

    printf '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n'
    printf '%s\n' "$label"
    printf '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n'
    printf \\n

    set +e
    "$@"
    status=$?
    set -e
    printf \\n

    if [ $status -ne 0 ]; then
        printf '%s\n' '⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️'
        printf '“%s” failed with status %d\n' "$label" $status
        printf '%s\n' '⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️　⚠️'

        # technically only correct when $@ yields WIFEXITED
        exit $status
    fi
    printf \\n
}
