#!/bin/sh
# usage: pipelines/build-nightly.sh

set -eu

. pipelines/step.sh
. pipelines/versions.sh
export RUSTFLAGS='--cfg ci_'

# check formatting in build-1.46.sh only,
# because rustfmt’s style changes over time

step 'run tests (#![no_std])' rustup run "$toolchain_nightly" \
    cargo test --no-default-features # NOT --workspace

step 'run tests (std)' rustup run "$toolchain_nightly" \
    cargo test --all-features --all-targets --workspace

step 'doc tests' rustup run "$toolchain_nightly" \
    cargo test --all-features --doc --workspace

step 'build docs' rustup run "$toolchain_nightly" \
    cargo doc --all-features --no-deps --document-private-items --workspace

step 'check no-{std,alloc}' rustup run "$toolchain_nightly" \
    sh -c 'cd nonono; cargo run -Z extra-link-arg'
