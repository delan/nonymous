#!/bin/sh

set -eu

. pipelines/versions.sh

if [ $# -gt 0 ]; then
    echo publishing "$1"
    rustup run "$toolchain_1_46" \
        cargo publish --token "${RELEASE_CARGO_TOKEN:?}" --manifest-path "$1/Cargo.toml"
else
    echo tagging
    a=$(mktemp)
    grep '^version' Cargo.toml | sed 1q > $a

    b=$(mktemp)
    cut -d '"' -f 2 $a > $b

    version="$(cat $b)"
    message="release $version [skip ci]"

    git commit -m "$message" --allow-empty

    # hg tag -u "${RELEASE_HG_USER:?}" -m "$message" -- "$version"
    git tag -am "$version" -- "$version"

    # # magic proxy feature doesn’t seem to support Mercurial
    # # https://community.atlassian.com/t5/x/x/ba-p/958407
    # hg push "ssh://hg@bitbucket.org/${BITBUCKET_REPO_FULL_NAME?:}"
    git push --tags origin "$(git symbolic-ref HEAD)"
fi
