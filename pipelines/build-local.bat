@echo off
rem usage: pipelines\build-local.bat





set RUSTFLAGS=--cfg ci_

rem check formatting in build-1.46.sh only,
rem because rustfmt’s style changes over time

echo run tests (#![no_std])
cargo test --no-default-features -q

echo run tests (std)
cargo test --features std --all-targets --workspace -q

echo doc tests
cargo test --features std --doc --workspace -q

echo build docs
cargo doc --features std --no-deps --document-private-items --workspace -q

echo test bore --decode
cargo run -p bore -- --decode < samples\daria.daz.cat.a.dns

echo test bore --show-resolvers
cargo run -p bore -- --show-resolvers
