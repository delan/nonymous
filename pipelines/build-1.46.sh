#!/bin/sh
# usage: pipelines/build-1.46.sh [coverage [upload]]

set -eu

. pipelines/step.sh
. pipelines/versions.sh
export RUSTFLAGS='--cfg ci_'

step 'check formatting' rustup run "$toolchain_1_46" \
    cargo fmt --all -- --check

step 'run tests (#![no_std])' rustup run "$toolchain_1_46" \
    cargo test --no-default-features # NOT --workspace

step 'run tests (std)' rustup run "$toolchain_1_46" \
    cargo test --features std --all-targets --workspace

step 'doc tests' rustup run "$toolchain_1_46" \
    cargo test --features std --doc --workspace

step 'build docs' rustup run "$toolchain_1_46" \
    cargo doc --features std --no-deps --document-private-items --workspace

if [ $# -gt 0 ] && [ _"$1" = _'coverage' ]; then
    step 'calculate code coverage' rustup run "$toolchain_1_46" \
        cargo tarpaulin --features std --all --out Xml

    if [ $# -gt 1 ] && [ _"$2" = _'upload' ]; then
        upload() {
            codecov=$(mktemp)
            curl -fsSo $codecov https://codecov.io/bash
            chmod +x $codecov
            $codecov
        }

        step 'upload code coverage' upload
    fi
fi
