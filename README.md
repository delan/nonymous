nonymous
========


[![crates.io](https://img.shields.io/crates/v/nonymous)](https://crates.io/crates/nonymous)
[![docs.rs](https://docs.rs/nonymous/badge.svg)](https://docs.rs/nonymous)


- [Release notes](https://bitbucket.org/delan/nonymous/src/default/RELEASES.md)


## Requirements


- Rust 1.57+ (tested with 1.57.0)


## Features


- ☑ view encoded protocol elements
- ☑ build queries and responses
- ☑ [bore(1) query tool](https://crates.io/crates/bore)
- ☐ read and write zone files
- ☐ stub resolver
- ☐ recursive resolver
- ☐ authoritative name server


## Non-goals


- [DNSSEC](https://sockpuppet.org/blog/2015/01/15/against-dnssec/)
