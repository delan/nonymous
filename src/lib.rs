#![forbid(unsafe_code)]
#![cfg_attr(ci_, deny(warnings))]
#![cfg_attr(all(test, feature = "bench"), feature(test))]
#![cfg_attr(not(feature = "std"), no_std)]
// #![warn(missing_docs)]

#[cfg(feature = "alloc")]
extern crate alloc;

macro_rules! error {
    ($name:ident $(($($before:ident, )* [$source:ident: $x:expr] $(, $after:ident)*))? $(, $variant:ident)*) => {
        #[cfg(feature = "std")]
        impl ::std::error::Error for $name {
            fn source(&self) -> Option<&(dyn ::std::error::Error + 'static)> {
                match self {
                    $($name($($before, )* $source $(, $after)*) => Some($x),)?
                    $($name::$variant(s) => Some(s),)*

                    #[allow(unreachable_patterns)]
                    _ => None,
                }
            }
        }
    };
}

// FIXME rust-lang/rust#67295
// #[cfg(any(test, doctest))]
#[macro_export]
#[doc(hidden)]
macro_rules! declare_any_error {
    (@real ($($variant:ident $ty:ty,)+)) => {
        #[derive(Debug)]
        pub enum AnyError {
            $($variant($ty),)+
        }

        $(impl From<$ty> for AnyError {
            fn from(e: $ty) -> Self { Self::$variant(e) }
        })+
    };

    (@real ($($done:tt)*) ($($path:tt)+)($name:ident), $($rest:tt)*) => {
        $crate::declare_any_error!(@real ($($done)* $name $($path)+::$name,) $($rest)*);
    };

    (@real ($($done:tt)*) ($($path:tt)+)($name:ident) as $variant:ident, $($rest:tt)*) => {
        $crate::declare_any_error!(@real ($($done)* $variant $($path)+::$name,) $($rest)*);
    };

    ($name:ident) => {
        $crate::declare_any_error!(
            @real ()
            (::core::convert)(Infallible),
            ($crate::view)(BoundsError),
            ($crate::view)(MessageError),
            ($crate::view)(QuestionError),
            ($crate::view)(RecordError),
            ($crate::view)(NameError),
            ($crate::view)(LabelError),
            ($crate::view)(ExtensionError),
            ($crate::view::rdata)(RdataError),
            ($crate::view::rdata)(SoaError),
            ($crate::emit)(SinkError),
            ($crate::emit)(GrowError),
            ($crate::emit::extension)(ExtensionError) as EmitExtensionError,
            ($crate::emit::message)(MessageError) as EmitMessageError,
            ($crate::emit::name)(NameError) as EmitNameError,
            ($crate::emit::question)(QuestionError) as EmitQuestionError,
            ($crate::emit::record)(RecordError) as EmitRecordError,
            ($crate::core)(OpcodeRangeError),
            ($crate::core)(RcodeRangeError),
            ($crate::core)(TypeFromStrError),
            ($crate::core)(ClassFromStrError),
            ($crate::core)(TtlFromStrError),
            ($crate::server)(ResponseError),
            (::core::fmt)(Error),
        );
    };
}

#[macro_use]
pub mod core;
pub mod emit;
pub mod fmt;
pub mod server;
pub mod view;

mod seen;

pub const MAX_SUPPORTED_EDNS_VERSION: u8 = 0;
