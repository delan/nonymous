use core::ops::Range;

use byteorder::{ByteOrder, NetworkEndian};

use super::{assert, BoundsError, CharacterString, CharacterStrings, Name, NameError, View};
use crate::{
    core::{Serial, Ttl},
    fmt::Format,
};

macro_rules! declare_rdata {
    ($($ty:ident,)*) => {
        #[non_exhaustive]
        pub enum Rdata<'s> {
            $($ty($ty<'s>),)*
        }
        $(impl<'s> From<$ty<'s>> for Rdata<'s> {
            fn from(inner: $ty<'s>) -> Self {
                Self::$ty(inner)
            }
        })*
        impl<'s> AsRef<dyn View<'s, Error = RdataError>> for Rdata<'s> {
            fn as_ref(&self) -> &dyn View<'s, Error = RdataError> {
                match self {
                    $(Self::$ty(x) => x,)*
                }
            }
        }
        impl<'s> AsRef<dyn Format + 's> for Rdata<'s> {
            fn as_ref(&self) -> &(dyn Format + 's) {
                match self {
                    $(Self::$ty(x) => x,)*
                }
            }
        }
    };
}

declare_rdata! {
    Malformed,
    Unknown,
    Soa,
    Mx,
    Txt,
    Caa,
    CompressibleName,
    InAddress,
    InAaaa,
}

pub struct Soa<'s> {
    pub source: &'s [u8],
    pub offset: usize,
    pub mname_len: usize,
    pub rname_len: usize,
}

pub struct Mx<'s> {
    pub source: &'s [u8],
    pub offset: usize,
    pub exchange_len: usize,
}

pub struct Txt<'s> {
    pub source: &'s [u8],
    pub offset: usize,
    pub len: usize,
}

pub struct Caa<'s> {
    pub source: &'s [u8],
    pub offset: usize,
    pub tag_len: usize,
    pub value_len: usize,
}

pub struct CaaTag<'s> {
    pub source: &'s [u8],
    pub offset: usize,
    pub len: usize,
}

pub struct CaaValue<'s> {
    pub source: &'s [u8],
    pub offset: usize,
    pub len: usize,
}

pub struct CompressibleName<'s> {
    pub name: Name<'s>,
}

pub struct InAddress<'s> {
    pub source: &'s [u8],
    pub offset: usize,
}

pub struct InAaaa<'s> {
    pub source: &'s [u8],
    pub offset: usize,
}

pub struct Malformed<'s> {
    pub inner: Unknown<'s>,
}

pub struct Unknown<'s> {
    pub source: &'s [u8],
    pub offset: usize,
    pub len: usize,
}

error!(RdataError([s: s.as_ref()], __));
#[derive(Debug, displaydoc::Display)]
/// failed to view RDATA ({1})
pub struct RdataError(RdataError0, &'static str);

#[cfg(feature = "std")]
pub type RdataError0 = eyre::Error;
#[cfg(not(feature = "std"))]
pub type RdataError0 = ();

error!(SoaError, Bounds, Name);
/// failed to view SOA RDATA
#[derive(Debug, displaydoc::Display)]
#[prefix_enum_doc_attributes]
pub enum SoaError {
    /// eof while viewing SERIAL or REFRESH or RETRY or EXPIRE or MINIMUM
    Bounds(BoundsError),

    /// SOA RDATA has malformed MNAME or RNAME
    Name(NameError),
}

impl RdataError {
    #[cfg(feature = "std")]
    fn new<E: 'static + Send + Sync + std::error::Error>(error: E, ty: &'static str) -> Self {
        Self(error.into(), ty)
    }
    #[cfg(not(feature = "std"))]
    fn new<E: 'static + Send + Sync>(_: E, ty: &'static str) -> Self {
        Self((), ty)
    }
}

trait RdataResultExt<T> {
    fn into_rdata_error<V>(self) -> Result<T, RdataError>;
}

#[cfg(feature = "std")]
impl<T, E: 'static + Send + Sync + std::error::Error> RdataResultExt<T> for Result<T, E> {
    fn into_rdata_error<V>(self) -> Result<T, RdataError> {
        self.map_err(|e| RdataError::new(e, core::any::type_name::<V>()))
    }
}
#[cfg(not(feature = "std"))]
impl<T, E: 'static + Send + Sync> RdataResultExt<T> for Result<T, E> {
    fn into_rdata_error<V>(self) -> Result<T, RdataError> {
        self.map_err(|e| RdataError::new(e, core::any::type_name::<V>()))
    }
}

impl Soa<'_> {
    pub fn mname(&self) -> Name {
        Name {
            source: self.source,
            offset: self.offset,
            len: self.mname_len,
        }
    }

    pub fn rname(&self) -> Name {
        Name {
            source: self.source,
            offset: self.offset + self.mname_len,
            len: self.rname_len,
        }
    }

    pub fn serial(&self) -> Serial {
        let offset = self.mname_len + self.rname_len;

        Serial::new(NetworkEndian::read_u32(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn refresh(&self) -> Ttl {
        let offset = self.mname_len + self.rname_len + 4;

        Ttl::new(NetworkEndian::read_u32(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn retry(&self) -> Ttl {
        let offset = self.mname_len + self.rname_len + 8;

        Ttl::new(NetworkEndian::read_u32(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn expire(&self) -> Ttl {
        let offset = self.mname_len + self.rname_len + 12;

        Ttl::new(NetworkEndian::read_u32(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn minimum(&self) -> Ttl {
        let offset = self.mname_len + self.rname_len + 16;

        Ttl::new(NetworkEndian::read_u32(
            &self.source[self.offset..][offset..],
        ))
    }
}

impl Mx<'_> {
    pub fn preference(&self) -> u16 {
        NetworkEndian::read_u16(&self.source[self.offset..])
    }

    pub fn exchange(&self) -> Name {
        Name {
            source: self.source,
            offset: self.offset + 2,
            len: self.exchange_len,
        }
    }
}

impl Txt<'_> {
    pub fn data(&self) -> CharacterStrings<'_> {
        let start = self.offset;
        let stop = self.offset + self.len;

        CharacterStrings::new(self.source, start..stop)
    }
}

impl Caa<'_> {
    pub fn flags(&self) -> u8 {
        self.source[self.offset]
    }

    pub fn critical(&self) -> bool {
        self.flags() >> 7 & 1 == 1
    }

    pub fn tag(&self) -> CaaTag {
        CaaTag {
            source: self.source,
            offset: self.offset + 2,
            len: self.tag_len,
        }
    }

    pub fn value(&self) -> CaaValue {
        CaaValue {
            source: self.source,
            offset: self.offset + 2 + self.tag_len,
            len: self.value_len,
        }
    }
}

impl<'s> View<'s> for Soa<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], range: Range<usize>) -> super::Result<Self, Self::Error> {
        let (_, rest) = Name::view(source, range.clone())
            .map_err(SoaError::Name)
            .into_rdata_error::<Self>()?;
        let mname_len = rest.start - range.start;

        let (_, mut rest) = Name::view(source, rest)
            .map_err(SoaError::Name)
            .into_rdata_error::<Self>()?;
        let rname_len = rest.start - range.start - mname_len;

        assert(source, &rest, 20)
            .map_err(SoaError::Bounds)
            .into_rdata_error::<Self>()?;
        rest.start += 20;

        Ok((
            Soa {
                source,
                offset: range.start,
                mname_len,
                rname_len,
            },
            rest,
        ))
    }

    fn len(&self) -> usize {
        self.mname_len + self.rname_len + 20
    }
}

impl<'s> View<'s> for Mx<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], range: Range<usize>) -> super::Result<Self, Self::Error> {
        let mut rest = range.clone();

        assert(source, &rest, 2).into_rdata_error::<Self>()?;
        rest.start += 2;

        let (_, rest) = Name::view(source, rest)
            .map_err(SoaError::Name)
            .into_rdata_error::<Self>()?;
        let exchange_len = rest.start - range.start - 2;

        Ok((
            Mx {
                source,
                offset: range.start,
                exchange_len,
            },
            rest,
        ))
    }

    fn len(&self) -> usize {
        2 + self.exchange_len
    }
}

impl<'s> View<'s> for Txt<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], range: Range<usize>) -> super::Result<Self, Self::Error> {
        let mut next = range.clone();

        while next.start != next.end {
            let (_, rest) = CharacterString::view(source, next).into_rdata_error::<Self>()?;
            next = rest;
        }

        let offset = range.start;
        let len = next.start - offset;

        Ok((
            Txt {
                source,
                offset,
                len,
            },
            next,
        ))
    }

    fn len(&self) -> usize {
        self.len
    }
}

impl<'s> View<'s> for Caa<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], range: Range<usize>) -> super::Result<Self, Self::Error> {
        let mut rest = range.clone();

        assert(source, &rest, 1).into_rdata_error::<Self>()?;
        rest.start += 1;

        let (octet, mut rest) = u8::view(source, rest).into_rdata_error::<Self>()?;
        let tag_len: usize = octet.into();

        assert(source, &rest, tag_len).into_rdata_error::<Self>()?;
        rest.start += tag_len;

        // value implicitly consumes the rest of the RDATA
        let value_len = rest.end - rest.start;
        rest.start += value_len;

        Ok((
            Caa {
                source,
                offset: range.start,
                tag_len,
                value_len,
            },
            rest,
        ))
    }

    fn len(&self) -> usize {
        1 + 1 + self.tag_len + self.value_len
    }
}

impl<'s> View<'s> for CompressibleName<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], range: Range<usize>) -> super::Result<Self, Self::Error> {
        let (name, rest) = Name::view(source, range).into_rdata_error::<Self>()?;

        Ok((Self { name }, rest))
    }

    fn len(&self) -> usize {
        self.name.len()
    }
}

impl<'s> View<'s> for InAddress<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], mut range: Range<usize>) -> super::Result<Self, Self::Error> {
        let offset = range.start;

        assert(source, &range, 4).into_rdata_error::<Self>()?;
        range.start += 4;

        Ok((InAddress { source, offset }, range))
    }

    fn len(&self) -> usize {
        4
    }
}

impl<'s> View<'s> for InAaaa<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], mut range: Range<usize>) -> super::Result<Self, Self::Error> {
        let offset = range.start;

        assert(source, &range, 16).into_rdata_error::<Self>()?;
        range.start += 16;

        Ok((InAaaa { source, offset }, range))
    }

    fn len(&self) -> usize {
        16
    }
}

impl<'s> View<'s> for Malformed<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], range: Range<usize>) -> super::Result<Self, Self::Error> {
        let (inner, range) = Unknown::view(source, range)?;

        Ok((Malformed { inner }, range))
    }

    fn len(&self) -> usize {
        self.inner.len()
    }
}

impl<'s> View<'s> for Unknown<'s> {
    type Error = RdataError;

    fn view_range(source: &'s [u8], mut range: Range<usize>) -> super::Result<Self, Self::Error> {
        let offset = range.start;
        let len = range.end - offset;

        assert(source, &range, len).into_rdata_error::<Self>()?;
        range.start += len;

        Ok((
            Unknown {
                source,
                offset,
                len,
            },
            range,
        ))
    }

    fn len(&self) -> usize {
        self.len
    }
}
