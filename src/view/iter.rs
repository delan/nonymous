use core::ops::Range;

use super::{CharacterString, Label, Question, Record, View};

/// An iterator over each [`Question`](crate::view::Question) in some range of a source, such as a question section.
pub struct Questions<'s> {
    source: &'s [u8],
    range: Range<usize>,
}

/// An iterator over each [`Record`](crate::view::Record) in some range of a source, such as an answer section.
pub struct Records<'s> {
    source: &'s [u8],
    range: Range<usize>,
}

/// An iterator over each [`Label`](crate::view::Label) of a [`Name`](crate::view::Name), after resolving any pointers.
pub struct Labels<'s> {
    source: &'s [u8],
    range: Range<usize>,
    current: Option<Label<'s>>,
}

/// An iterator over each [`CharacterString`](crate::view::CharacterString) of a [`Txt`](crate::view::rdata::Txt).
pub struct CharacterStrings<'s> {
    source: &'s [u8],
    range: Range<usize>,
}

impl<'s> Questions<'s> {
    pub fn new(source: &'s [u8], range: Range<usize>) -> Self {
        Self { source, range }
    }
}

impl<'s> Records<'s> {
    pub fn new(source: &'s [u8], range: Range<usize>) -> Self {
        Self { source, range }
    }
}

impl<'s> Labels<'s> {
    pub fn new(source: &'s [u8], range: Range<usize>) -> Self {
        Self {
            source,
            range,
            current: None,
        }
    }
}

impl<'s> CharacterStrings<'s> {
    pub fn new(source: &'s [u8], range: Range<usize>) -> Self {
        Self { source, range }
    }
}

impl<'s> Iterator for Questions<'s> {
    type Item = Question<'s>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Ok((question, rest)) = Question::view(self.source, self.range.clone()) {
            self.range = rest;

            Some(question)
        } else {
            None
        }
    }
}

impl<'s> Iterator for Records<'s> {
    type Item = Record<'s>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Ok((record, rest)) = Record::view(self.source, self.range.clone()) {
            self.range = rest;

            Some(record)
        } else {
            None
        }
    }
}

impl<'s> Iterator for Labels<'s> {
    type Item = Label<'s>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current.as_ref().map_or(false, |x| x.null()) {
            return None;
        }

        while let Ok((label, rest)) = Label::view(self.source, self.range.clone()) {
            if let Some(offset) = label.pointer() {
                self.range = offset.into()..self.source.len();
            } else {
                self.range = rest;
                self.current = Some(label);
                return self.current.clone(); // FIXME?
            }
        }

        None
    }
}

impl<'s> Iterator for CharacterStrings<'s> {
    type Item = CharacterString<'s>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Ok((string, rest)) = CharacterString::view(self.source, self.range.clone()) {
            self.range = rest;

            Some(string)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use assert_matches::assert_matches;

    use super::super::{Label, Name, View};

    declare_any_error!(AnyError);

    #[test]
    fn labels() -> Result<(), AnyError> {
        let source = b"\x05daria\x03daz\x03cat\0\x08charming\xC0\x06";
        let (name, _) = Name::view(source, 15..26)?;
        let mut labels = name.labels();

        assert_matches!(
            labels.next(),
            Some(Label {
                source: _,
                offset: 15,
                len: 9,
            })
        );
        assert_matches!(
            labels.next(),
            Some(Label {
                source: _,
                offset: 6,
                len: 4,
            })
        );
        assert_matches!(
            labels.next(),
            Some(Label {
                source: _,
                offset: 10,
                len: 4,
            })
        );
        assert_matches!(
            labels.next(),
            Some(Label {
                source: _,
                offset: 14,
                len: 1,
            })
        );
        assert_matches!(labels.next(), None);

        Ok(())
    }
}
