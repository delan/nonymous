use core::{cmp::Ordering, ops::Add};

use crate::{
    core::{Serial, SerialAdditionError},
    view::Label,
};

impl PartialEq<[u8]> for Label<'_> {
    fn eq(&self, other: &[u8]) -> bool {
        self.literal()
            .map_or(false, |x| x.eq_ignore_ascii_case(other))
    }
}

impl<const N: usize> PartialEq<[u8; N]> for Label<'_> {
    fn eq(&self, other: &[u8; N]) -> bool {
        self.literal()
            .map_or(false, |x| x.eq_ignore_ascii_case(other))
    }
}

// FIXME this non-transitive relation violates the contract of PartialOrd
impl PartialOrd<Serial> for Serial {
    fn partial_cmp(&self, other: &Serial) -> Option<Ordering> {
        const HALF: u32 = 1 << u32::BITS - 1;

        match (self.value(), other.value()) {
            (x, y) if x == y => Some(Ordering::Equal),
            (x, y) if x < y && y - x < HALF => Some(Ordering::Less),
            (x, y) if x > y && x - y > HALF => Some(Ordering::Less),
            (x, y) if x < y && y - x > HALF => Some(Ordering::Greater),
            (x, y) if x > y && x - y < HALF => Some(Ordering::Greater),
            _ => None,
        }
    }
}

impl Add<u32> for Serial {
    type Output = Result<Self, SerialAdditionError>;

    fn add(self, rhs: u32) -> Self::Output {
        if rhs > (1 << u32::BITS - 1) - 1 {
            return Err(SerialAdditionError);
        }

        Ok(Self::new(self.value().wrapping_add(rhs)))
    }
}

#[cfg(test)]
mod test {
    use crate::core::{Serial, SerialAdditionError};

    #[test]
    fn serial() {
        // RFC 1982
        assert_eq!(
            Serial::new(0xFFFFFFFF) + 0x7FFFFFFF,
            Ok(Serial::new(0x7FFFFFFE))
        );
        assert_eq!(
            Serial::new(0xFFFFFFFF) + 0x80000000,
            Err(SerialAdditionError)
        );
        assert!(Serial::new(0x40000000) < Serial::new(0xBFFFFFFF));
        assert!(Serial::new(0x40000000)
            .partial_cmp(&Serial::new(0xC0000000))
            .is_none());
        assert!(Serial::new(0x40000000) > Serial::new(0xC0000001));
        assert!(Serial::new(0xC0000000) < Serial::new(0x3FFFFFFF));
        assert!(Serial::new(0xC0000000)
            .partial_cmp(&Serial::new(0x40000000))
            .is_none());
        assert!(Serial::new(0xC0000000) > Serial::new(0x40000001));
    }
}
