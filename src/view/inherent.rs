use core::ops::Range;

use byteorder::{ByteOrder, NetworkEndian};

use crate::core::{Class, Opcode, Rcode, Ttl, Type};
use crate::MAX_SUPPORTED_EDNS_VERSION;

use super::iter::{Labels, Questions, Records};
use super::rdata::{
    Caa, CompressibleName, InAaaa, InAddress, Malformed, Mx, Rdata, RdataError, Soa, Txt, Unknown,
};
use super::ExtensionError;
use super::{CharacterString, Extension, Header, Label, Message, Name, Question, Record, View};

impl Message<'_> {
    pub fn header(&self) -> Header {
        Header {
            source: self.source,
            offset: self.offset,
        }
    }

    pub fn opt(&self) -> Option<&Extension> {
        self.opt.as_ref()
    }

    pub fn rcode(&self) -> Rcode {
        let basic_part = self.header().rcode();
        let extended_part = self.opt().map_or(0, |x| x.extended());

        Rcode::from_basic_extended(basic_part, extended_part).expect("guaranteed by Header impl")
    }

    pub fn qd(&self) -> Questions<'_> {
        let start = self.offset + self.header().len();
        let stop = start + self.qd_len;

        Questions::new(self.source, start..stop)
    }

    pub fn an(&self) -> Records<'_> {
        let start = self.offset + self.header().len() + self.qd_len;
        let stop = start + self.an_len;

        Records::new(self.source, start..stop)
    }

    pub fn ns(&self) -> Records<'_> {
        let start = self.offset + self.header().len() + self.qd_len + self.an_len;
        let stop = start + self.ns_len;

        Records::new(self.source, start..stop)
    }

    pub fn ar(&self) -> Records<'_> {
        let start = self.offset + self.header().len() + self.qd_len + self.an_len + self.ns_len;
        let stop = start + self.ar_len;

        Records::new(self.source, start..stop)
    }

    pub fn udp_limit(&self) -> u16 {
        self.opt().map_or(512, |x| x.udp().max(512))
    }
}

impl Header<'_> {
    pub fn id(&self) -> u16 {
        NetworkEndian::read_u16(&self.source[self.offset..][0..])
    }

    pub fn qr(&self) -> bool {
        self.source[self.offset..][2] >> 7 & 1 == 1
    }

    pub fn opcode(&self) -> Opcode {
        Opcode::new(self.source[self.offset..][2] >> 3 & 0xF).unwrap()
    }

    pub fn aa(&self) -> bool {
        self.source[self.offset..][2] >> 2 & 1 == 1
    }

    pub fn tc(&self) -> bool {
        self.source[self.offset..][2] >> 1 & 1 == 1
    }

    pub fn rd(&self) -> bool {
        self.source[self.offset..][2] >> 0 & 1 == 1
    }

    pub fn ra(&self) -> bool {
        self.source[self.offset..][3] >> 7 & 1 == 1
    }

    fn rcode(&self) -> u8 {
        self.source[self.offset..][3] & 0xF
    }

    pub fn qdcount(&self) -> u16 {
        NetworkEndian::read_u16(&self.source[self.offset..][4..])
    }

    pub fn ancount(&self) -> u16 {
        NetworkEndian::read_u16(&self.source[self.offset..][6..])
    }

    pub fn nscount(&self) -> u16 {
        NetworkEndian::read_u16(&self.source[self.offset..][8..])
    }

    pub fn arcount(&self) -> u16 {
        NetworkEndian::read_u16(&self.source[self.offset..][10..])
    }
}

impl Question<'_> {
    pub fn qname(&self) -> Name {
        Name {
            source: self.source,
            offset: self.offset,
            len: self.len - 4,
        }
    }

    pub fn qtype(&self) -> Type {
        let offset = self.len - 4;

        Type::new(NetworkEndian::read_u16(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn qclass(&self) -> Class {
        let offset = self.len - 2;

        Class::new(NetworkEndian::read_u16(
            &self.source[self.offset..][offset..],
        ))
    }
}

impl Record<'_> {
    pub fn name(&self) -> Name {
        Name {
            source: self.source,
            offset: self.offset,
            len: self.name_len,
        }
    }

    pub fn r#type(&self) -> Type {
        let offset = self.name_len;

        Type::new(NetworkEndian::read_u16(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn class(&self) -> Class {
        let offset = self.name_len + 2;

        Class::new(NetworkEndian::read_u16(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn ttl(&self) -> Ttl {
        let offset = self.name_len + 4;

        Ttl::new(NetworkEndian::read_u32(
            &self.source[self.offset..][offset..],
        ))
    }

    pub fn rdlength(&self) -> u16 {
        let offset = self.offset + self.name_len + 8;

        NetworkEndian::read_u16(&self.source[offset..])
    }

    pub fn try_rdata(&self) -> Result<Rdata, RdataError> {
        let range = self.rdata_range();

        Ok(match (self.class().value(), self.r#type().value()) {
            (_, 6) => Soa::view(self.source, range)?.0.into(),
            (_, 15) => Mx::view(self.source, range)?.0.into(),
            (_, 16) => Txt::view(self.source, range)?.0.into(),
            (_, 257) => Caa::view(self.source, range)?.0.into(),
            // RFC 3597 § 4
            (_, 2) | (_, 5) | (_, 12) => CompressibleName::view(self.source, range)?.0.into(),
            (1, 1) => InAddress::view(self.source, range)?.0.into(),
            (1, 28) => InAaaa::view(self.source, range)?.0.into(),
            _ => Unknown::view(self.source, range)?.0.into(),
        })
    }

    pub fn rdata(&self) -> Rdata {
        if let Ok(result) = self.try_rdata() {
            return result;
        }

        Malformed::view(self.source, self.rdata_range())
            .expect("guaranteed by impl View for Record")
            .0
            .into()
    }

    pub fn rdata_range(&self) -> Range<usize> {
        let start = self.offset + self.name_len + 10;
        let stop = start + self.rdata_len;

        start..stop
    }
}

impl Name<'_> {
    pub fn root(&self) -> bool {
        self.labels()
            .next()
            .expect("Labels guarantees that this is infallible")
            .null()
    }

    pub fn labels(&self) -> Labels<'_> {
        let start = self.offset;
        let stop = self.offset + self.len;

        Labels::new(self.source, start..stop)
    }
}

impl<'s> Label<'s> {
    pub fn null(&self) -> bool {
        self.len == 1
    }

    pub fn pointer(&self) -> Option<u16> {
        if self.source[self.offset] < 0xC0 {
            return None;
        }

        Some(NetworkEndian::read_u16(&self.source[self.offset..]) ^ 0xC000)
    }

    pub fn literal(&self) -> Option<&'s [u8]> {
        if self.source[self.offset] >= 0xC0 {
            return None;
        }

        if self.null() {
            return None;
        }

        Some(&self.source[self.offset..][1..self.len])
    }
}

impl<'s> Extension<'s> {
    pub fn wrap(inner: Record<'s>) -> Result<Self, ExtensionError> {
        if !inner.name().root() {
            // FIXME rust-lang/rust#8995
            return Err(ExtensionError::BadName);
        }

        let result = Self { inner };

        if result.version() > MAX_SUPPORTED_EDNS_VERSION {
            return Err(ExtensionError::UnimplementedVersion);
        }

        Ok(result)
    }

    pub fn udp(&self) -> u16 {
        self.inner.class().value()
    }

    pub fn extended(&self) -> u8 {
        // the & 0xFF makes the as u8 safe
        (self.inner.ttl().value() >> 24 & 0xFF) as u8
    }

    pub fn version(&self) -> u8 {
        // the & 0xFF makes the as u8 safe
        (self.inner.ttl().value() >> 16 & 0xFF) as u8
    }

    pub fn r#do(&self) -> bool {
        self.inner.ttl().value() >> 15 & 1 == 1
    }
}

impl<'s> CharacterString<'s> {
    pub fn value(&self) -> &[u8] {
        &self.source[self.offset..][1..self.len]
    }
}

#[cfg(test)]
mod test {
    use super::super::{Label, Name, View};

    declare_any_error!(AnyError);

    #[test]
    fn name() -> Result<(), AnyError> {
        let source = b"\0";
        let (name, _) = Name::view(source, 0..1)?;
        assert_eq!(name.root(), true);

        let source = b"\0\xC0\x00";
        let (name, _) = Name::view(source, 1..3)?;
        assert_eq!(name.root(), true);

        Ok(())
    }

    #[test]
    fn label() -> Result<(), AnyError> {
        let source = b"\0";
        let (label, _) = Label::view(source, 0..1)?;
        assert_eq!(label.null(), true);

        let source = b"\0\xC0\x00";
        let (label, _) = Label::view(source, 1..3)?;
        assert_eq!(label.null(), false);

        Ok(())
    }
}
