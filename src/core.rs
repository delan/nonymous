//! Primitive protocol types ([`Serial`], [`Ttl`]) and named constants ([`Opcode`], [`Rcode`], [`Type`], [`Class`]).

mod r#const;

pub use self::r#const::{NamedConstants, NamedRcodes, CLASS, OPCODE, RCODE, TYPE};

use core::str::FromStr;

/// Gets the [`Opcode`] for the given literal name, at compile time.
#[macro_export]
macro_rules! opcode {
    ($name:literal) => {{
        const VALUE: u16 = match $crate::core::OPCODE.to_number($name) {
            Some(x) => x,
            None => panic!("unknown OPCODE name"),
        };
        // FIXME rust#87852
        const RESULT: u8 = if VALUE > 0xFF {
            panic!("OPCODE value out of range")
        } else {
            VALUE as _
        };
        match $crate::core::Opcode::new(RESULT) {
            Err(_) => panic!("invalid OPCODE value"),
            Ok(x) => x,
        }
    }};
}

/// Gets the [`Rcode`] for the given literal name, at compile time.
#[macro_export]
macro_rules! rcode {
    ($name:literal) => {{
        const VALUE: u16 = match $crate::core::RCODE.to_number($name) {
            Some(x) => x,
            None => panic!("unknown RCODE name"),
        };
        match $crate::core::Rcode::new(VALUE) {
            Err(_) => panic!("invalid RCODE value"),
            Ok(x) => x,
        }
    }};
}

/// Gets the [`Type`] for the given literal name, at compile time.
#[macro_export]
macro_rules! r#type {
    ($name:literal) => {{
        const VALUE: u16 = match $crate::core::TYPE.to_number($name) {
            Some(x) => x,
            None => panic!("unknown TYPE name"),
        };
        $crate::core::Type::new(VALUE)
    }};
}

/// Gets the [`Class`] for the given literal name, at compile time.
#[macro_export]
macro_rules! class {
    ($name:literal) => {{
        const VALUE: u16 = match $crate::core::CLASS.to_number($name) {
            Some(x) => x,
            None => panic!("unknown CLASS name"),
        };
        $crate::core::Class::new(VALUE)
    }};
}

error!(OpcodeRangeError);
#[derive(Debug, displaydoc::Display)]
/// OPCODE out of range: {0}
pub struct OpcodeRangeError(u8);

error!(RcodeRangeError);
#[derive(Debug, displaydoc::Display)]
/// RCODE out of range
pub enum RcodeRangeError {
    /// extended RCODE exceeds 12 bits: {0}
    Extended(u16),

    /// non-extended RCODE exceeds 4 bits: {0}
    Basic(u8),
}

error!(TypeFromStrError);
#[derive(Debug, displaydoc::Display)]
/// unrecognised TYPE name
pub struct TypeFromStrError;

error!(ClassFromStrError);
#[derive(Debug, displaydoc::Display)]
/// unrecognised CLASS name
pub struct ClassFromStrError;

error!(TtlFromStrError);
#[derive(Debug, displaydoc::Display)]
/// failed to parse TTL value
pub struct TtlFromStrError;

error!(SerialAdditionError);
#[derive(Debug, displaydoc::Display, PartialEq)]
/// serial number addition overflow
pub struct SerialAdditionError;

#[derive(Debug, Clone, PartialEq)]
pub struct Opcode(u8);

#[derive(Debug, Clone, PartialEq)]
pub struct Rcode(u16);

#[derive(Debug, Clone, PartialEq)]
pub struct Type(u16);

#[derive(Debug, Clone, PartialEq)]
pub struct Class(u16);

#[derive(Debug, Clone)]
pub struct Ttl(u32);

#[derive(Debug, Clone, PartialEq)]
pub struct Serial(u32);

impl Opcode {
    pub const fn new(value: u8) -> Result<Self, OpcodeRangeError> {
        if value > 0xF {
            return Err(OpcodeRangeError(value));
        }

        Ok(Self(value))
    }

    pub const fn value(&self) -> u8 {
        self.0
    }
}

impl Rcode {
    pub const fn new(value: u16) -> Result<Self, RcodeRangeError> {
        if value > 0xFFF {
            return Err(RcodeRangeError::Extended(value));
        }

        Ok(Self(value))
    }

    pub fn from_basic_extended(basic_part: u8, extended_part: u8) -> Result<Self, RcodeRangeError> {
        if basic_part > 0xF {
            return Err(RcodeRangeError::Basic(basic_part));
        }

        Ok(Self::new(
            u16::from(extended_part) << 4 | u16::from(basic_part),
        )?)
    }

    pub const fn name_opt(&self) -> Option<&'static str> {
        RCODE.to_name_opt(self.value())
    }

    pub const fn name_tsig(&self) -> Option<&'static str> {
        RCODE.to_name_tsig(self.value())
    }

    pub const fn value(&self) -> u16 {
        self.0
    }

    pub const fn basic_part(&self) -> u8 {
        // FIXME rust#87852
        // (self.0 & 0xF).try_into().expect("guaranteed by Self::of")
        (self.0 & 0xF) as u8
    }

    pub const fn extended_part(&self) -> u8 {
        // FIXME rust#87852
        // (self.0 >> 4).try_into().expect("guaranteed by Self::of")
        (self.0 >> 4) as u8
    }
}

impl Type {
    pub fn new(value: u16) -> Self {
        Self(value)
    }

    pub fn value(&self) -> u16 {
        self.0
    }
}

impl Class {
    pub fn new(value: u16) -> Self {
        Self(value)
    }

    pub fn value(&self) -> u16 {
        self.0
    }
}

impl Ttl {
    pub fn new(value: u32) -> Self {
        Self(value)
    }

    pub fn value(&self) -> u32 {
        self.0
    }

    pub(crate) fn edns_version(&self) -> u8 {
        (self.value() >> 16 & 0xFF) as _
    }
}

impl Serial {
    pub fn new(value: u32) -> Self {
        Self(value)
    }

    pub fn value(&self) -> u32 {
        self.0
    }
}

impl FromStr for Type {
    type Err = TypeFromStrError;

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        if let Some(value) = TYPE.to_number(&source) {
            return Ok(Type(value));
        }

        // RFC 3597 § 5
        if source.starts_with("TYPE") {
            return Ok(Type(
                u16::from_str_radix(&source[4..], 10).map_err(|_| TypeFromStrError)?,
            ));
        }

        Err(TypeFromStrError)
    }
}

impl FromStr for Class {
    type Err = ClassFromStrError;

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        if let Some(value) = CLASS.to_number(&source) {
            return Ok(Class(value));
        }

        // RFC 3597 § 5
        if source.starts_with("CLASS") {
            return Ok(Class(
                u16::from_str_radix(&source[5..], 10).map_err(|_| ClassFromStrError)?,
            ));
        }

        Err(ClassFromStrError)
    }
}

#[cfg(all(test, feature = "bench"))]
mod bench {
    mod parse {
        extern crate test;

        use test::Bencher;

        use crate::core::{Class, Type};

        #[bench]
        fn r#type(bencher: &mut Bencher) {
            bencher.iter(|| "cname".parse::<Type>());
        }

        #[bench]
        fn class(bencher: &mut Bencher) {
            bencher.iter(|| "hs".parse::<Class>());
        }
    }
}
