macro_rules! search {
    ($xs:expr, $get:expr, $cmp:expr, $x:expr) => {{
        use ::core::cmp::Ordering;

        let mut h = 0;
        let mut i = 0;
        let mut j = $xs.len();

        while j - h > 0 {
            i = h + (j - h) / 2;
            match (j - h, $cmp($get($xs[i]), $x)) {
                (_, Ordering::Equal) => break,
                (1, _) => return None,
                (_, Ordering::Less) => h = i,
                (_, Ordering::Greater) => j = i,
            }
        }

        while i < $xs.len() && matches!($cmp($get($xs[i]), $x), Ordering::Equal) {
            i += 1;
        }

        // always return last matching entry
        Some($xs[i - 1])
    }};
}

#[cfg(any(test, feature = "bench"))]
macro_rules! linear {
    ($xs:expr, $get:expr, $cmp:expr, $x:expr) => {{
        use ::core::cmp::Ordering;

        let mut i = 0;

        loop {
            if i >= $xs.len() {
                break None;
            } else if let Ordering::Equal = $cmp($get($xs[i]), $x) {
                break Some($xs[i]);
            }
            i += 1;
        }
    }};
}
