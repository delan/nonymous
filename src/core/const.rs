#[macro_use]
mod heap;

#[macro_use]
mod search;

use core::cmp::Ordering;

type I = usize;
type K = u16;
type V<'v> = &'v str;
type Pair = (K, V<'static>);
type Entry = (I, K, V<'static>);

/// A pair of sorted mappings, one from names to numbers, one from numbers to names.
pub struct NamedConstants {
    ks: &'static [Entry],
    vs: &'static [Entry],
}

/// [`NamedConstants`] for RCODE, with support for the context-sensitive value 16 (BADVERS or BADSIG).
pub struct NamedRcodes(NamedConstants);

impl NamedConstants {
    pub const fn to_name(&self, k: K) -> Option<V> {
        match search!(self.ks, get_k, cmp_k, k) {
            Some(entry) => Some(get_v(entry)),
            _ => None,
        }
    }

    #[allow(unused)]
    #[cfg(any(test, feature = "bench"))]
    const fn to_name_linear(&self, k: K) -> Option<V> {
        match linear!(self.ks, get_k, cmp_k, k) {
            Some(entry) => Some(get_v(entry)),
            _ => None,
        }
    }

    pub const fn to_number(&self, v: V) -> Option<K> {
        match search!(self.vs, get_v, cmp_v, v) {
            Some(entry) => Some(get_k(entry)),
            _ => None,
        }
    }

    #[allow(unused)]
    #[cfg(any(test, feature = "bench"))]
    const fn to_number_linear(&self, v: V) -> Option<K> {
        match linear!(self.vs, get_v, cmp_v, v) {
            Some(entry) => Some(get_k(entry)),
            _ => None,
        }
    }
}

impl NamedRcodes {
    pub const fn to_name_opt(&self, k: K) -> Option<V> {
        match k {
            16 => Some("BADVERS"),
            _ => self.0.to_name(k),
        }
    }

    pub const fn to_name_tsig(&self, k: K) -> Option<V> {
        match k {
            16 => Some("BADSIG"),
            _ => self.0.to_name(k),
        }
    }

    pub const fn to_number(&self, v: V) -> Option<K> {
        self.0.to_number(v)
    }
}

macro_rules! replace {
    ($old:tt $new:expr) => {
        $new
    };
}

macro_rules! count {
    ($($token:tt)*) => {<[()]>::len(&[$(replace!($token ())),*])};
}

macro_rules! names {
    (@ $vis:vis $name:ident[$n:expr] [$($pairs:expr,)*]) => {
        ::paste::paste! {
            const [<$name _ARRAY>]: ([Entry; $n], [Entry; $n]) = {
                // explicit type provides error message for bad N in [T; N]
                let pairs: [Pair; $n] = [$($pairs,)*];
                let mut ks = [(0, 0, ""); $n];
                let mut vs = [(0, 0, ""); $n];

                // temporary lengths for heap insert and sort (extract)
                let mut kn = 0;
                let mut vn = 0;

                let mut i = 0;
                while i < pairs.len() {
                    let (k, v) = pairs[i];
                    let entry = (i, k, v);
                    insert!(ks, kn, cmp_ki, entry);
                    insert!(vs, vn, cmp_vi, entry);
                    i += 1;
                }

                drain!(ks, kn, cmp_ki);
                drain!(vs, vn, cmp_vi);

                (ks, vs)
            };

            $vis const $name: NamedConstants = NamedConstants {
                ks: &[<$name _ARRAY>].0,
                vs: &[<$name _ARRAY>].1,
            };
        }
    };
    ($vis:vis $name:ident [$($pairs:expr,)*]) => {
        names!(@ $vis $name[count!($(($pairs))*)] [$($pairs,)*]);
    };
}

names!(pub OPCODE [
    // RFC 6895 § 2.2
    (0, "Query"),
    (1, "IQuery"),
    (2, "Status"),
    (4, "Notify"),
    (5, "Update"),
    (6, "DSO"),
]);

names!(RCODE_ [
    // RFC 6895 § 2.3
    (0, "NoError"),
    (1, "FormErr"),
    (2, "ServFail"),
    (3, "NXDomain"),
    (4, "NotImp"),
    (5, "Refused"),
    (6, "YXDomain"),
    (7, "YXRRSet"),
    (8, "NXRRSet"),

    // Not Authoritative (RFC 2136)
    // • when message has no TSIG RR
    // • when TSIG RR has zero Error field
    // Not Authorized (RFC 2845)
    // • when TSIG RR has non-zero Error field
    (9, "NotAuth"),

    (10, "NotZone"),
    (11, "DSOTYPENI"),

    // Bad OPT Version (RFC 6891)
    // • when found in OPT RR
    (16, "BADVERS"),
    // TSIG Signature Failure (RFC 2845)
    // • when found in TSIG RR
    (16, "BADSIG"),

    (17, "BADKEY"),
    (18, "BADTIME"),
    (19, "BADMODE"),
    (20, "BADNAME"),
    (21, "BADALG"),
    (22, "BADTRUNC"),
    (23, "BADCOOKIE"),
]);

pub const RCODE: NamedRcodes = NamedRcodes(RCODE_);

names!(pub TYPE [
    // RFC 1035 § 3.2.2
    (1, "A"),
    (2, "NS"),
    (3, "MD"),
    (4, "MF"),
    (5, "CNAME"),
    (6, "SOA"),
    (7, "MB"),
    (8, "MG"),
    (9, "MR"),
    (10, "NULL"),
    (11, "WKS"),
    (12, "PTR"),
    (13, "HINFO"),
    (14, "MINFO"),
    (15, "MX"),
    (16, "TXT"),
    (17, "RP"), //
    (18, "AFSDB"), //
    (19, "X25"), //
    (20, "ISDN"), //
    (21, "RT"), //
    (22, "NSAP"), //
    (23, "NSAP-PTR"), //
    (24, "SIG"), //
    (25, "KEY"), //
    (26, "PX"), //
    (27, "GPOS"), //
    // RFC 3596 § 2.1
    (28, "AAAA"),
    (29, "LOC"), //
    (30, "NXT"), //
    (31, "EID"), //
    (32, "NIMLOC"), //
    (33, "SRV"), //
    (34, "ATMA"), //
    (35, "NAPTR"), //
    (36, "KX"), //
    (37, "CERT"), //
    (38, "A6"), //
    (39, "DNAME"), //
    (40, "SINK"), //
    (41, "OPT"), //
    (42, "APL"), //
    (43, "DS"), //
    (44, "SSHFP"), //
    (45, "IPSECKEY"), //
    (46, "RRSIG"), //
    (47, "NSEC"), //
    (48, "DNSKEY"), //
    (49, "DHCID"), //
    (50, "NSEC3"), //
    (51, "NSEC3PARAM"), //
    (52, "TLSA"), //
    (53, "SMIMEA"), //
    // (54, "Unassigned"), //
    (55, "HIP"), //
    (56, "NINFO"), //
    (57, "RKEY"), //
    (58, "TALINK"), //
    (59, "CDS"), //
    (60, "CDNSKEY"), //
    (61, "OPENPGPKEY"), //
    (62, "CSYNC"), //
    (63, "ZONEMD"), //
    (64, "SVCB"), //
    (65, "HTTPS"), //
    (99, "SPF"), //
    (100, "UINFO"), //
    (101, "UID"), //
    (102, "GID"), //
    (103, "UNSPEC"), //
    (104, "NID"), //
    (105, "L32"), //
    (106, "L64"), //
    (107, "LP"), //
    (108, "EUI48"), //
    (109, "EUI64"), //
    (249, "TKEY"), //
    (250, "TSIG"), //
    (251, "IXFR"), //
    // RFC 1035 § 3.2.3
    (252, "AXFR"),
    (253, "MAILB"),
    (254, "MAILA"),
    (255, "*"),
    // RFC 6195 § 3.1
    (255, "ALL"),
    // RFC 8482 § 1.1
    (255, "ANY"),
    (256, "URI"), //
    // RFC 6844 § 7.1
    (257, "CAA"),
    (258, "AVC"), //
    (259, "DOA"), //
    (260, "AMTRELAY"), //
    (32768, "TA"), //
    (32769, "DLV"), //
]);

names!(pub CLASS [
    // RFC 1035 § 3.2.4
    (1, "IN"),
    (2, "CS"),
    (3, "CH"),
    (4, "HS"),
    // RFC 1035 § 3.2.5
    (255, "*"),
    // RFC 6195 § 3.2
    (255, "ANY"),
]);

macro_rules! then {
    ($first:expr $($(, $rest:expr)+)?) => {
        match $first {
            $(::core::cmp::Ordering::Equal => then!($($rest),*),)?
            x => x,
        }
    };
}

const fn get_i(x: Entry) -> I {
    x.0
}

const fn get_k(x: Entry) -> K {
    x.1
}

const fn get_v(x: Entry) -> V<'static> {
    x.2
}

const fn cmp_ki(p: Entry, q: Entry) -> Ordering {
    then!(cmp_k(get_k(p), get_k(q)), cmp_i(get_i(p), get_i(q)))
}

const fn cmp_vi(p: Entry, q: Entry) -> Ordering {
    then!(cmp_v(get_v(p), get_v(q)), cmp_i(get_i(p), get_i(q)))
}

const fn cmp_i(p: I, q: I) -> Ordering {
    if p < q {
        Ordering::Less
    } else if p > q {
        Ordering::Greater
    } else {
        Ordering::Equal
    }
}

const fn cmp_k(p: K, q: K) -> Ordering {
    if p < q {
        Ordering::Less
    } else if p > q {
        Ordering::Greater
    } else {
        Ordering::Equal
    }
}

const fn cmp_v(p: V, q: V) -> Ordering {
    let p = p.as_bytes();
    let q = q.as_bytes();
    let mut i = 0;

    loop {
        match (i >= p.len(), i >= q.len()) {
            (true, true) => break,
            (true, _) => return Ordering::Less,
            (_, true) => return Ordering::Greater,
            (_, _) => {}
        }

        let p = p[i].to_ascii_uppercase();
        let q = q[i].to_ascii_uppercase();

        if p < q {
            return Ordering::Less;
        } else if p > q {
            return Ordering::Greater;
        }

        i += 1;
    }

    Ordering::Equal
}

#[cfg(test)]
mod test {
    use super::{CLASS, RCODE, TYPE};

    #[test]
    fn any() {
        // RFC 8482 § 1.1
        assert_eq!(TYPE.to_name(255), Some("ANY"));

        // RFC 6195 § 3.2
        assert_eq!(CLASS.to_name(255), Some("ANY"));
    }

    #[test]
    fn number() {
        assert_eq!(TYPE.to_number("cname"), Some(5));
        assert_eq!(TYPE.to_number_linear("cname"), Some(5));
        assert_eq!(TYPE.to_number("mx"), Some(15));
        assert_eq!(TYPE.to_number_linear("mx"), Some(15));
        assert_eq!(TYPE.to_number("wks"), Some(11));
        assert_eq!(TYPE.to_number_linear("wks"), Some(11));

        // RFC 6895 § 2.3
        assert_eq!(RCODE.to_number("badvers"), Some(16));
        assert_eq!(RCODE.to_number("badsig"), Some(16));
    }

    #[test]
    fn name() {
        assert_eq!(TYPE.to_name(5), Some("CNAME"));
        assert_eq!(TYPE.to_name_linear(5), Some("CNAME"));
        assert_eq!(TYPE.to_name(11), Some("WKS"));
        assert_eq!(TYPE.to_name_linear(11), Some("WKS"));
        assert_eq!(TYPE.to_name(15), Some("MX"));
        assert_eq!(TYPE.to_name_linear(15), Some("MX"));
        assert_eq!(TYPE.to_name(28), Some("AAAA"));
        assert_eq!(TYPE.to_name_linear(28), Some("AAAA"));
        assert_eq!(TYPE.to_name(257), Some("CAA"));
        assert_eq!(TYPE.to_name_linear(257), Some("CAA"));

        // RFC 6895 § 2.3
        assert_eq!(RCODE.to_name_opt(16), Some("BADVERS"));
        assert_eq!(RCODE.to_name_tsig(16), Some("BADSIG"));
    }

    #[cfg(feature = "std")]
    #[test]
    #[ntest::timeout(5000)]
    fn infinite() {
        use super::*;
        let search = 9; // greater than last
        names!(ODD [ (1, "a"), (2, "b"), (3, "c"), ]);
        assert_eq!(ODD.to_name(search), None);
        names!(EVEN [ (1, "a"), (2, "b"), (3, "c"), (4, "d"), ]);
        assert_eq!(EVEN.to_name(search), None);
    }
}

#[cfg(all(test, feature = "bench"))]
#[rustfmt::skip]
mod bench {
    extern crate test;

    use test::Bencher;
    use test::bench::{black_box as b};

    use super::{TYPE, NamedConstants, Entry, Pair, cmp_ki, cmp_vi};

    //  11
    //  83
    #[bench] fn bee_name_binary(bencher: &mut Bencher) { bencher.iter(|| BEE.to_name(b(429))); }
    #[bench] fn bee_name_linear(bencher: &mut Bencher) { bencher.iter(|| BEE.to_name_linear(b(429))); }

    //  51
    //  1089
    #[bench] fn bee_number_binary(bencher: &mut Bencher) { bencher.iter(|| BEE.to_number(b("dies"))); }
    #[bench] fn bee_number_linear(bencher: &mut Bencher) { bencher.iter(|| BEE.to_number_linear(b("dies"))); }

    //  11  12  7   8   4
    //  2   2   3   5   16
    #[bench] fn name_binary0(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name(b(5))); }
    #[bench] fn name_binary1(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name(b(11))); }
    #[bench] fn name_binary2(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name(b(15))); }
    #[bench] fn name_binary3(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name(b(28))); }
    #[bench] fn name_binary4(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name(b(257))); }
    #[bench] fn name_linear0(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name_linear(b(5))); }
    #[bench] fn name_linear1(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name_linear(b(11))); }
    #[bench] fn name_linear2(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name_linear(b(15))); }
    #[bench] fn name_linear3(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name_linear(b(28))); }
    #[bench] fn name_linear4(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_name_linear(b(257))); }

    //  35  33  28
    //  42  103 164
    #[bench] fn number_binary0(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_number(b("cname"))); }
    #[bench] fn number_binary1(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_number(b("mx"))); }
    #[bench] fn number_binary2(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_number(b("wks"))); }
    #[bench] fn number_linear0(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_number_linear(b("cname"))); }
    #[bench] fn number_linear1(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_number_linear(b("mx"))); }
    #[bench] fn number_linear2(bencher: &mut Bencher) { bencher.iter(|| TYPE.to_number_linear(b("wks"))); }

    // curl https://gist.githubusercontent.com/MattIPv4/045239bc27b16b2bcf7a3a9a4648c08a/raw \
    // | tr A-Z a-z | tr -cs a-z \\n | sort -u | nl | awk '{print$1,$2}' \
    // | sed -E 's/(.*) (.*)/        (\1, "\2"),/'
    names!(BEE [
        (1, "a"),
        (2, "able"),
        (3, "abort"),
        (4, "aborting"),
        (5, "about"),
        (6, "absolutely"),
        (7, "absurd"),
        (8, "according"),
        (9, "account"),
        (10, "across"),
        (11, "action"),
        (12, "actor"),
        (13, "actual"),
        (14, "actually"),
        (15, "adam"),
        (16, "addicted"),
        (17, "adjusted"),
        (18, "adrenaline"),
        (19, "ads"),
        (20, "advancement"),
        (21, "advantage"),
        (22, "advisory"),
        (23, "affect"),
        (24, "affects"),
        (25, "affirmative"),
        (26, "afraid"),
        (27, "after"),
        (28, "afternoon"),
        (29, "aftertaste"),
        (30, "again"),
        (31, "against"),
        (32, "agreed"),
        (33, "ahead"),
        (34, "aim"),
        (35, "aiming"),
        (36, "air"),
        (37, "airport"),
        (38, "alaska"),
        (39, "alert"),
        (40, "alive"),
        (41, "all"),
        (42, "allergic"),
        (43, "allow"),
        (44, "almost"),
        (45, "alone"),
        (46, "already"),
        (47, "also"),
        (48, "always"),
        (49, "am"),
        (50, "amazing"),
        (51, "amen"),
        (52, "amusement"),
        (53, "an"),
        (54, "anchor"),
        (55, "and"),
        (56, "angel"),
        (57, "anger"),
        (58, "angry"),
        (59, "animal"),
        (60, "animals"),
        (61, "another"),
        (62, "ant"),
        (63, "antennae"),
        (64, "antennas"),
        (65, "antonio"),
        (66, "anxiously"),
        (67, "any"),
        (68, "anybody"),
        (69, "anyone"),
        (70, "anything"),
        (71, "anyway"),
        (72, "anywhere"),
        (73, "approach"),
        (74, "appropriate"),
        (75, "approved"),
        (76, "are"),
        (77, "area"),
        (78, "aren"),
        (79, "around"),
        (80, "arousing"),
        (81, "artie"),
        (82, "artificial"),
        (83, "as"),
        (84, "ask"),
        (85, "asked"),
        (86, "aspect"),
        (87, "associate"),
        (88, "assume"),
        (89, "assuming"),
        (90, "at"),
        (91, "athletic"),
        (92, "attack"),
        (93, "attempting"),
        (94, "attendant"),
        (95, "attention"),
        (96, "attorney"),
        (97, "attracted"),
        (98, "authorities"),
        (99, "autograph"),
        (100, "automatic"),
        (101, "automatically"),
        (102, "autopilot"),
        (103, "available"),
        (104, "aviation"),
        (105, "aware"),
        (106, "away"),
        (107, "awful"),
        (108, "awfully"),
        (109, "awkward"),
        (110, "b"),
        (111, "babbling"),
        (112, "baby"),
        (113, "back"),
        (114, "backhand"),
        (115, "bad"),
        (116, "badfella"),
        (117, "balance"),
        (118, "bald"),
        (119, "ball"),
        (120, "balloon"),
        (121, "balm"),
        (122, "band"),
        (123, "barely"),
        (124, "barricade"),
        (125, "barrier"),
        (126, "barry"),
        (127, "base"),
        (128, "bats"),
        (129, "battery"),
        (130, "be"),
        (131, "beams"),
        (132, "bear"),
        (133, "beards"),
        (134, "bears"),
        (135, "beast"),
        (136, "beautiful"),
        (137, "because"),
        (138, "bed"),
        (139, "bedbug"),
        (140, "bee"),
        (141, "beekeeper"),
        (142, "beekeepers"),
        (143, "been"),
        (144, "beep"),
        (145, "beer"),
        (146, "bees"),
        (147, "before"),
        (148, "begin"),
        (149, "begins"),
        (150, "behind"),
        (151, "being"),
        (152, "bejesus"),
        (153, "belated"),
        (154, "believe"),
        (155, "believed"),
        (156, "belong"),
        (157, "benefit"),
        (158, "benson"),
        (159, "bent"),
        (160, "best"),
        (161, "bet"),
        (162, "better"),
        (163, "between"),
        (164, "big"),
        (165, "billion"),
        (166, "birds"),
        (167, "birth"),
        (168, "bit"),
        (169, "biting"),
        (170, "black"),
        (171, "blacktop"),
        (172, "blade"),
        (173, "blend"),
        (174, "blew"),
        (175, "blinked"),
        (176, "blood"),
        (177, "bloome"),
        (178, "blossom"),
        (179, "blow"),
        (180, "blows"),
        (181, "blue"),
        (182, "bluffing"),
        (183, "boat"),
        (184, "bob"),
        (185, "body"),
        (186, "bogus"),
        (187, "book"),
        (188, "boots"),
        (189, "bored"),
        (190, "born"),
        (191, "borrow"),
        (192, "both"),
        (193, "bothering"),
        (194, "bottom"),
        (195, "bounty"),
        (196, "bouquets"),
        (197, "bowl"),
        (198, "box"),
        (199, "boy"),
        (200, "boys"),
        (201, "brain"),
        (202, "brave"),
        (203, "brazenly"),
        (204, "bread"),
        (205, "break"),
        (206, "breakfast"),
        (207, "breaking"),
        (208, "breath"),
        (209, "breathe"),
        (210, "breaths"),
        (211, "bred"),
        (212, "briefcase"),
        (213, "bright"),
        (214, "bring"),
        (215, "bringing"),
        (216, "britches"),
        (217, "brochure"),
        (218, "brooch"),
        (219, "brooms"),
        (220, "bubble"),
        (221, "bud"),
        (222, "buddy"),
        (223, "bug"),
        (224, "bugging"),
        (225, "bugs"),
        (226, "build"),
        (227, "building"),
        (228, "bumble"),
        (229, "bumbleton"),
        (230, "business"),
        (231, "businesses"),
        (232, "busted"),
        (233, "busy"),
        (234, "but"),
        (235, "buttocks"),
        (236, "buzz"),
        (237, "buzzwell"),
        (238, "buzzy"),
        (239, "by"),
        (240, "bye"),
        (241, "cab"),
        (242, "cafeteria"),
        (243, "cake"),
        (244, "california"),
        (245, "call"),
        (246, "called"),
        (247, "calories"),
        (248, "campaign"),
        (249, "camps"),
        (250, "can"),
        (251, "candy"),
        (252, "cannonball"),
        (253, "cannot"),
        (254, "capable"),
        (255, "captain"),
        (256, "capture"),
        (257, "car"),
        (258, "card"),
        (259, "care"),
        (260, "career"),
        (261, "careful"),
        (262, "carefully"),
        (263, "carl"),
        (264, "carob"),
        (265, "case"),
        (266, "casually"),
        (267, "catch"),
        (268, "catches"),
        (269, "cause"),
        (270, "celebrate"),
        (271, "celebrating"),
        (272, "celery"),
        (273, "center"),
        (274, "century"),
        (275, "ceremonies"),
        (276, "chance"),
        (277, "change"),
        (278, "changes"),
        (279, "chapstick"),
        (280, "charges"),
        (281, "check"),
        (282, "cheering"),
        (283, "cheese"),
        (284, "chemical"),
        (285, "children"),
        (286, "chill"),
        (287, "chips"),
        (288, "choice"),
        (289, "choices"),
        (290, "choose"),
        (291, "chopsticks"),
        (292, "chung"),
        (293, "church"),
        (294, "churning"),
        (295, "chute"),
        (296, "cicada"),
        (297, "cinnabon"),
        (298, "cinnamon"),
        (299, "circular"),
        (300, "circumstances"),
        (301, "city"),
        (302, "class"),
        (303, "classy"),
        (304, "clean"),
        (305, "clear"),
        (306, "clients"),
        (307, "cloning"),
        (308, "close"),
        (309, "closed"),
        (310, "clothes"),
        (311, "cloudy"),
        (312, "coaster"),
        (313, "cockpit"),
        (314, "coffee"),
        (315, "collectively"),
        (316, "collector"),
        (317, "college"),
        (318, "color"),
        (319, "colored"),
        (320, "combined"),
        (321, "come"),
        (322, "comes"),
        (323, "coming"),
        (324, "common"),
        (325, "community"),
        (326, "compadres"),
        (327, "companies"),
        (328, "company"),
        (329, "compelling"),
        (330, "compete"),
        (331, "competition"),
        (332, "complete"),
        (333, "completely"),
        (334, "comrades"),
        (335, "concentrate"),
        (336, "concludes"),
        (337, "congratulations"),
        (338, "consider"),
        (339, "considered"),
        (340, "conspiracy"),
        (341, "constantly"),
        (342, "contoured"),
        (343, "contraption"),
        (344, "control"),
        (345, "controls"),
        (346, "cool"),
        (347, "coolest"),
        (348, "cooling"),
        (349, "coordinator"),
        (350, "copilot"),
        (351, "copy"),
        (352, "corrected"),
        (353, "correctly"),
        (354, "cotton"),
        (355, "couch"),
        (356, "could"),
        (357, "couldn"),
        (358, "counting"),
        (359, "county"),
        (360, "couple"),
        (361, "course"),
        (362, "coursing"),
        (363, "court"),
        (364, "cousin"),
        (365, "cousins"),
        (366, "covered"),
        (367, "crashing"),
        (368, "crazy"),
        (369, "cream"),
        (370, "creatures"),
        (371, "creep"),
        (372, "crew"),
        (373, "cricket"),
        (374, "crime"),
        (375, "crossed"),
        (376, "crowds"),
        (377, "crud"),
        (378, "crumb"),
        (379, "cry"),
        (380, "culture"),
        (381, "cup"),
        (382, "cups"),
        (383, "cut"),
        (384, "cute"),
        (385, "d"),
        (386, "da"),
        (387, "dad"),
        (388, "dada"),
        (389, "daffodil"),
        (390, "daisies"),
        (391, "damage"),
        (392, "dangerous"),
        (393, "dash"),
        (394, "date"),
        (395, "dated"),
        (396, "dating"),
        (397, "dave"),
        (398, "dawg"),
        (399, "day"),
        (400, "days"),
        (401, "dead"),
        (402, "deadified"),
        (403, "deady"),
        (404, "deal"),
        (405, "dean"),
        (406, "death"),
        (407, "decide"),
        (408, "decision"),
        (409, "decisions"),
        (410, "deck"),
        (411, "dee"),
        (412, "degrees"),
        (413, "delay"),
        (414, "deli"),
        (415, "demand"),
        (416, "denouncing"),
        (417, "deny"),
        (418, "depends"),
        (419, "desk"),
        (420, "destruction"),
        (421, "detail"),
        (422, "developing"),
        (423, "devilishly"),
        (424, "diabolical"),
        (425, "did"),
        (426, "didn"),
        (427, "die"),
        (428, "died"),
    ]);
}
