use arrayvec::ArrayVec;

/// A fast set of message compression offsets (RFC 1035 § 4.1.4).
///
/// We use these sets to detect pointer cycles in [`Name` views](`crate::view::Name`).
#[derive(Default)]
pub struct Seen(ArrayVec<u8, { 16384 / 8 }>);

impl Seen {
    /// Marks the given `offset` as seen. Returns `true` iff the offset was already marked as seen.
    pub fn see(&mut self, offset: u16) -> bool {
        let offset = usize::from(offset);
        let (index, shift) = (offset / 8, offset % 8);

        let len = self.0.len();

        for _ in len..(index + 1) {
            self.0.push(0);
        }

        let old = self.0[index];
        let new = old | 1 << shift;

        self.0[index] = new;

        new == old
    }
}
