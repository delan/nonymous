//! Usage:
//!
//! ```sh
//! $ bore --encode -x 10.42.69.0 | cargo run --example dynrdns --features std | bore --decode
//! ```

extern crate eyre;

use std::io::{stdin, stdout, Read, Write};

use nonymous::{class, emit::name::NameBuilder, opcode, r#type, rcode, server::response};

fn main() -> eyre::Result<()> {
    let mut query = vec![];
    stdin().lock().read_to_end(&mut query)?;

    let response = rdns_response(&query)?;
    stdout().lock().write_all(&response)?;

    Ok(())
}

fn rdns_response(query: &[u8]) -> eyre::Result<Vec<u8>> {
    let mut result = vec![];

    if let Some((query, response)) = response(&query, &mut result, false)? {
        if query.header().opcode() != opcode!("Query") || query.header().qdcount() != 1 {
            response.rcode(rcode!("NotImp")).finish()?.finish();
            return Ok(result);
        }

        let question = query.qd().next().unwrap();
        if question.qclass() != class!("IN") || question.qtype() != r#type!("PTR") {
            response.rcode(rcode!("Refused")).finish()?.finish();
            return Ok(result);
        }

        let qname = question.qname();
        let labels = qname.labels().flat_map(|x| x.literal()).collect::<Vec<_>>();
        let name = match labels[..] {
            [.., b"in-addr", b"arpa"] => {
                if labels.len() != 4 + 2 {
                    response.rcode(rcode!("Refused")).finish()?.finish();
                    return Ok(result);
                }
                let octets = labels
                    .iter()
                    .take(4)
                    .rev()
                    .flat_map(|x| std::str::from_utf8(x))
                    .flat_map(|x| x.parse::<u8>())
                    .map(|x| x.to_string())
                    .collect::<Vec<_>>();
                if octets.len() != 4 {
                    response.rcode(rcode!("Refused")).finish()?.finish();
                    return Ok(result);
                }
                let result = octets.join("-");

                format!("dyn-{}.isp.example.", result)
            }
            [.., b"ip6", b"arpa"] => {
                if labels.len() != 32 + 2 {
                    response.rcode(rcode!("Refused")).finish()?.finish();
                    return Ok(result);
                }
                let nibbles = labels
                    .iter()
                    .take(32)
                    .rev()
                    .flat_map(|x| std::str::from_utf8(x))
                    .flat_map(|x| u16::from_str_radix(x, 16))
                    .filter(|&x| x <= 0xF)
                    .collect::<Vec<_>>();
                if nibbles.len() != 32 {
                    response.rcode(rcode!("Refused")).finish()?.finish();
                    return Ok(result);
                }
                let result = nibbles
                    .chunks_exact(4)
                    .map(|x| x.iter().fold(0, |a, x| a << 4 | x))
                    .map(|x| format!("{:x}", x))
                    .collect::<Vec<_>>()
                    .join("-");

                format!("dyn-{}.isp.example.", result)
            }
            _ => {
                response.rcode(rcode!("Refused")).finish()?.finish();
                return Ok(result);
            }
        };

        let mut response = response
            .rcode(rcode!("NoError"))
            .copy_question(&question)?
            .into_an()
            .record()?
            .name()?;
        for label in &labels {
            response = response.label(label)?;
        }

        response
            .try_into_rdata()?
            .class(class!("IN"))
            .r#type(r#type!("PTR"))
            .build_rdata::<NameBuilder<_>>()?
            .labels(name.as_bytes())?
            .return_to_rdata()?
            .finish()?
            .finish()?
            .finish();
    };

    Ok(result)
}
