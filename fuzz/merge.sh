#!/bin/sh
# usage: fuzz/merge.sh <fuzz target> <source ...>
set -eu
fuzz_target=$1; shift

i=0; while [ $i -lt $# ]; do
    set -- "$@" "fuzz/corpus/$1"
    shift
    i=$((i+1))
done

mkdir -p fuzz/corpus/$fuzz_target
cargo fuzz run $fuzz_target -- -merge=1 fuzz/corpus/$fuzz_target "$@"
