Set up your environment:


```
rustup default nightly
rustup component add llvm-tools-preview
cargo install cargo-fuzz rustfilt
nix-shell -p binutils stdenv{,.cc}
```


Start fuzzing:


```
fuzz/merge.sh view_plain ../../samples
fuzz/run.sh view_plain
fuzz/minimise.sh view_plain
fuzz/coverage.sh view_plain x86_64-unknown-linux-gnu
```


Inspect inputs:


```
cargo run -p bore -- --decode < fuzz/corpus/view_plain/0123456789abcdef0123456789abcdef01234567
```
