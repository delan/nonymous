#![no_main]
use libfuzzer_sys::fuzz_target;
use nonymous::view::{Message, View};
use nonymous::fmt::Plain;

fuzz_target!(|data: &[u8]| {
    if let Ok((message, _)) = Message::view(data, ..) {
        let _ = format!("{}", Plain(&message));
    }
});
