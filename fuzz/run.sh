#!/bin/sh
# usage: fuzz/run.sh <fuzz target>
set -eu
fuzz_target=$1; shift
processors=$(< /proc/cpuinfo grep '^processor[^ -~]*:' | wc -l)

cargo fuzz run $fuzz_target -- -jobs=$((processors/2))
