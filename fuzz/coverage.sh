#!/bin/sh
# usage: fuzz/coverage.sh <fuzz target> <target triple>
set -eu
fuzz_target=$1; shift
target_triple=$1; shift

cargo fuzz coverage $fuzz_target
~/.rustup/toolchains/nightly-$target_triple/lib/rustlib/$target_triple/bin/llvm-cov show -Xdemangler=rustfilt fuzz/target/$target_triple/release/$fuzz_target -format=html -instr-profile=fuzz/coverage/$fuzz_target/coverage.profdata > fuzz/coverage.html

printf '\n\n>>> %s\n' 'fuzz/coverage.html'
