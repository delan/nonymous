#!/bin/sh
# usage: fuzz/minimise.sh <fuzz target>
set -eu
fuzz_target=$1; shift

mkdir fuzz/corpus/$fuzz_target.old  # fail if exists
mv fuzz/corpus/$fuzz_target/* fuzz/corpus/$fuzz_target.old
fuzz/merge.sh $fuzz_target $fuzz_target.old
rm -R fuzz/corpus/$fuzz_target.old
